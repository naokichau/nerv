import { NervnetProvider } from './../../providers/nervnet/nervnet';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the MyProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-profile',
  templateUrl: 'my-profile.html',
})
export class MyProfilePage {
  account: any
  accountImg: any
  constructor(private navCtrl: NavController, public navParams: NavParams, nervnet: NervnetProvider) {
    this.account = nervnet.account
    this.accountImg = nervnet.accountImg
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyProfilePage');
  }

  signData() {
    this.navCtrl.push("SignDataPage")
  }
  viewPubkey() {
    this.navCtrl.push("UserPubkeyPage")
  }
}
