import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BackupRestorePage } from './backup-restore';

@NgModule({
  declarations: [
    BackupRestorePage,
  ],
  imports: [
    IonicPageModule.forChild(BackupRestorePage),
  ],
})
export class BackupRestorePageModule {}
