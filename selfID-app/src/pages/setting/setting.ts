import { NervnetProvider } from './../../providers/nervnet/nervnet';
import { WelcomePage } from './../welcome/welcome';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public nervnet: NervnetProvider, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }
  viewNodes() {
    this.navCtrl.push("NodeListPage")
  }
  viewPrivateKey() {

  }
  connectNetwork() {
    this.nervnet.ConnectNetwork()
  }
  switchID() {
    let prompt = this.alertCtrl.create({
      title: 'Confirmation',
      subTitle: 'Are you sure you want to switch account?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Yes',
          handler: () => {
            let loading = this.loadingCtrl.create({
              content: 'Logout...'
            });

            loading.present();
            setTimeout(() => {
              loading.dismiss()
              this.nervnet.UnloadAcc()
            }, 800);
          }
        }
      ]
    });
    prompt.present();
  }
  viewBackupRestore() {
    this.navCtrl.push("BackupRestorePage")
  }
}
