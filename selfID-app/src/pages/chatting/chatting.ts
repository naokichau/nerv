import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NervnetProvider } from '../../providers/nervnet/nervnet';

@IonicPage()
@Component({
  selector: 'page-chatting',
  templateUrl: 'chatting.html',
})
export class ChattingPage {
  conversationHistory: any;
  conversationID: any;
  userMsgInput: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public nervnet: NervnetProvider) {

  }

  ionViewDidLoad() {
    this.conversationID = this.navParams.data.data
    this.conversationHistory = this.nervnet.accountMsg.get(this.conversationID)
  }

  sendMsg() {
    if (this.userMsgInput.trim() != "") {
      this.nervnet.SendMsgTo(this.conversationID, this.userMsgInput)
      this.userMsgInput = ""
    }
  }

  updateConversation() {
    this.conversationHistory = this.nervnet.accountMsg.get(this.conversationID)
  }
}
