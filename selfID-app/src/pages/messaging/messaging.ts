import { NervnetProvider } from './../../providers/nervnet/nervnet';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import blockies from "../../assets/scripts/blockies.js"

/**
 * Generated class for the MessagingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messaging',
  templateUrl: 'messaging.html',
})
export class MessagingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public nervnet: NervnetProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagingPage');
  }

  createChat() {
    let prompt = this.alertCtrl.create({
      title: 'Your friend address',
      subTitle: 'Add your friend address and start chatting',
      inputs: [
        {
          name: 'pubkey',
          placeholder: 'address'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Start',
          handler: data => {
            this.nervnet.SendMsgTo(data.pubkey, "hello")
          }
        }
      ]
    });
    prompt.present();
  }
  viewChat(conversation) {
    this.navCtrl.push("ChattingPage", { data: conversation })
  }

  deleteCvs(conversation) {

  }

  getMsgHistory() {
    var msgList = Array.from(this.nervnet.accountMsg.entries())
    msgList.forEach(conversation => {
      conversation[1] = conversation[1][conversation[1].length - 1]
      conversation[2] = blockies.create({
        seed: conversation[0],
        size: 8,
        scale: 9
      }).toDataURL()
    });
    return msgList;
  }
  scanQR() {

  }

  searchChat() {

  }
}
