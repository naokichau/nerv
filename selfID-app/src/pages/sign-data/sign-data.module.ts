import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignDataPage } from './sign-data';

@NgModule({
  declarations: [
    SignDataPage,
  ],
  imports: [
    IonicPageModule.forChild(SignDataPage),
  ],
})
export class SignDataPageModule {}
