import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateAppCertPage } from './create-app-cert';

@NgModule({
  declarations: [
    CreateAppCertPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateAppCertPage),
  ],
})
export class CreateAppCertPageModule {}
