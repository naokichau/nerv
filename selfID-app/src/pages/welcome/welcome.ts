import { NervnetProvider } from './../../providers/nervnet/nervnet';
import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public nervnet: NervnetProvider, private alertCtrl: AlertController, public loadingCtrl: LoadingController) {
  }

  CreateID() {
    let loading = this.loadingCtrl.create({
      content: 'Creating account...'
    });

    loading.present();
    setTimeout(() => {
      this.nervnet.CreateAcc().then(result => {
        loading.dismiss();
        this.navCtrl.setRoot(TabsPage)
      })
    }, 1000);

  }

  RecoverID() {
    let prompt = this.alertCtrl.create({
      title: 'Recover ID',
      subTitle: 'Enter your private key to recover your ID',
      inputs: [
        {
          name: 'privkey',
          placeholder: 'Private key'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Recover',
          handler: data => {
            let loading = this.loadingCtrl.create({
              content: 'Importing account...'
            });

            loading.present();
            setTimeout(() => {
              this.nervnet.Import(data.privkey).then(result => {
                loading.dismiss();
                if (result != false) {
                  this.navCtrl.setRoot(TabsPage)
                }
                else {
                  alert("Import account failed!")
                }
              })
            }, 800);
          }
        }
      ]
    });
    prompt.present();
  }

}
