import { NervnetProvider } from './../../providers/nervnet/nervnet';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the NodeListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-node-list',
  templateUrl: 'node-list.html',
})
export class NodeListPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public nervnet: NervnetProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NodeListPage');
  }
  addNode() {
    let prompt = this.alertCtrl.create({
      title: 'Add nodes',
      subTitle: 'Enter node links. ',
      inputs: [
        {
          name: 'nodelinks',
          placeholder: 'e.g 192.168.0.35:8080'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Ping',
          handler: data => {
            var links = []
            links.push(data.nodelinks)
            this.nervnet.PingNodes(links).then(result => {
              console.log(result)
            })
          }
        }
      ]
    });
    prompt.present();
  }
  copyNodeList() {

  }
  getEntries(): Array<[string, any]> {
    return Array.from(this.nervnet.networkNodes.entries());
  }
  refreshNodeList(){
    this.nervnet.refreshNodeList()
  }
}
