import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  aRoot = 'MyProfilePage'
  bRoot = 'AppStorePage'
  cRoot = 'MessagingPage'
  dRoot = 'SettingPage'

  constructor(public navCtrl: NavController) { }

}
