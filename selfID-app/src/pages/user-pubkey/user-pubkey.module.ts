import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserPubkeyPage } from './user-pubkey';

@NgModule({
  declarations: [
    UserPubkeyPage,
  ],
  imports: [
    IonicPageModule.forChild(UserPubkeyPage),
  ],
})
export class UserPubkeyPageModule {}
