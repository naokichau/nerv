import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AppStorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-app-store',
  templateUrl: 'app-store.html',
})
export class AppStorePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  createAppCert() {
    this.navCtrl.push("CreateAppCertPage")
  }

}
