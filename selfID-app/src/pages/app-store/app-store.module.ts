import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppStorePage } from './app-store';

@NgModule({
  declarations: [
    AppStorePage,
  ],
  imports: [
    IonicPageModule.forChild(AppStorePage),
  ],
})
export class AppStorePageModule {}
