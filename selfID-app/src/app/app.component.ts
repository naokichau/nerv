import { NervnetProvider } from './../providers/nervnet/nervnet';
import { WelcomePage } from './../pages/welcome/welcome';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = WelcomePage;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, nervnet: NervnetProvider, public events: Events) {
    platform.ready().then(() => {
      nervnet.LoadAcc().then(result => {
        if (result) {
          this.rootPage = TabsPage
        }
      })
      statusBar.styleDefault();
      splashScreen.hide();
    });
    events.subscribe('unloadAcc', () => {
      this.nav.setRoot(WelcomePage)
    });
  }
}
