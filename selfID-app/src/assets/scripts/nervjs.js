(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('crypto'), require('secp256k1'), require('eccrypto'), require('request'), require('events')) :
    typeof define === 'function' && define.amd ? define(['crypto', 'secp256k1', 'eccrypto', 'request', 'events'], factory) :
    (global.nervjs = factory(null, null, null, null, null));
}(this, (function (crypto, secp256k1, eccrypto, request, events) {
  'use strict';

  crypto = crypto && crypto.hasOwnProperty('default') ? crypto['default'] : crypto;
  secp256k1 = secp256k1 && secp256k1.hasOwnProperty('default') ? secp256k1['default'] : secp256k1;
  eccrypto = eccrypto && eccrypto.hasOwnProperty('default') ? eccrypto['default'] : eccrypto;
  request = request && request.hasOwnProperty('default') ? request['default'] : request;
  events = events && events.hasOwnProperty('default') ? events['default'] : events;

  function Import(privKey) {
    return new Promise(function (resolve, reject) {
      const pubKey = secp256k1.publicKeyCreate(Buffer.from(privKey, 'base64'));
      var mykey = {
        privKey: privKey,
        pubKey: pubKey.toString("base64")
      };
      resolve(mykey);
    })
  }

  function GenerateAcc() {
    return new Promise((resolve, reject) => {
      let privKey;
      do {
        privKey = crypto.randomBytes(32);
      } while (!secp256k1.privateKeyVerify(privKey))

      // get the public key in a compressed format
      const pubKey = secp256k1.publicKeyCreate(privKey);
      var mykey = {
        privKey: privKey.toString("base64"),
        pubKey: pubKey.toString("base64")
      };
      resolve(mykey);
    })
  }

  function Sign(data, privKey) {
    return new Promise((resolve, reject) => {
      try {
        const sigObj = secp256k1.sign(Buffer.from(data, 'base64'), Buffer.from(privKey, 'base64'));
        resolve(sigObj.signature.toString("base64"));
      } catch (error) {
        console.log(error);
      }
    })
  }

  function Verify(data, signature, publicKey) {
    return new Promise((resolve, reject) => {
      resolve(secp256k1.verify(Buffer.from(data, 'base64'), Buffer.from(signature, 'base64'), Buffer.from(publicKey, 'base64')));
    })
  }

  // function GetPubKey(data, signature) {
  //     return new Promise((resolve, reject) => {
  //         var key1 = secp256k1.recover(Buffer.from(data, 'base64'), Buffer.from(signature, 'base64'), 2, false)

  //         console.log(secp256k1.publicKeyVerify(key1), secp256k1.publicKeyConvert(key1, true).toString("base64"))
  //     })
  // }
  var accountFn = {
    Sign,
    Import,
    GenerateAcc,
    Verify
  };

  function EncryptWithPubKey(data, pubKey) {
    return new Promise((resolve, reject) => {
      var publicKeyA = secp256k1.publicKeyConvert(Buffer.from(pubKey, "base64"), false);
      eccrypto.encrypt(publicKeyA, Buffer(data)).then(encrypted => {
        encrypted.iv = encrypted.iv.toString("base64");
        encrypted.ephemPublicKey = encrypted.ephemPublicKey.toString("base64");
        encrypted.ciphertext = encrypted.ciphertext.toString("base64");
        encrypted.mac = encrypted.mac.toString("base64");
        resolve(Buffer.from(JSON.stringify(encrypted)).toString("base64"));
      });
    })
  }

  function DecryptWithPrivKey(data, privKey) {
    return new Promise((resolve, reject) => {
      var encrypted = JSON.parse(Buffer.from(data, "base64"));
      encrypted.iv = Buffer.from(encrypted.iv, "base64");
      encrypted.ephemPublicKey = Buffer.from(encrypted.ephemPublicKey, "base64");
      encrypted.ciphertext = Buffer.from(encrypted.ciphertext, "base64");
      encrypted.mac = Buffer.from(encrypted.mac, "base64");
      eccrypto.decrypt(Buffer.from(privKey, 'base64'), encrypted).then(decrypted => {
        resolve(decrypted.toString());
      });
    })
  }

  function Hash(data) {
    return new Promise((resolve, reject) => {
      var sha256 = crypto.createHash('sha256').update(data).digest("base64");
      resolve(sha256);
    })
  }
  var assetFn = {
    EncryptWithPubKey,
    DecryptWithPrivKey,
    Hash
  };

  var nodeConnections = new Map();
  var globalEvent;
  var account = {
    KeyPair: {},
    Create: () => {
      return new Promise((resolve, reject) => {
        accountFn.GenerateAcc().then(result => {
          account.KeyPair = result;
          resolve(account.KeyPair);
        });
      })
    },
    Sign: (data) => {
      return new Promise((resolve, reject) => {
        accountFn.Sign(data, account.KeyPair.privKey).then(result => {
          resolve(result);
        });
      })
    },
    Verify: (data, sig, publicKey) => {
      return new Promise((resolve, reject) => {
        accountFn.Verify(data, sig, publicKey).then(result => {
          resolve(result);
        });
      })
    },
    GetPubKey: (data, sig) => {
      return new Promise((resolve, reject) => {
        accountFn.GetPubKey(data, sig).then(result => {
          resolve(result);
        });
      })
    },
    Import: (privKey) => {
      return new Promise((resolve, reject) => {
        accountFn.Import(privKey).then(result => {
          account.KeyPair = result;
          resolve(account.KeyPair);
        });
      })
    },
    CreateCert: (appPolicy, expireTime) => {
      return new Promise(function (resolve, reject) {
        var preSignCert = {
          user: account.KeyPair.pubKey,
          app: appPolicy.appID,
          dataPolicy: appPolicy.dataPolicy,
          expireTime: expireTime,
          signTime: Date.now()
        };
        var preSignCertStr = JSON.stringify(preSignCert);
        assetFn.Hash(preSignCertStr).then(dataHash => {
          account.Sign(dataHash).then(result => {
            resolve(result);
          });
        });
      })
    },
    SendMsgto: (receiverAddr, msg) => {
      return new Promise((resolve, reject) => {
        var receiverAddrHEX = new Buffer(receiverAddr, 'base64').toString('hex');
        var dataPkgCtn = {
          id: crypto.randomBytes(16).toString("hex"),
          t: 2,
          c: Buffer.from(msg).toString("base64"),
          r: receiverAddrHEX,
          s: account.KeyPair.pubKey,
          st: Date.now().toString()
        };
        const dataPkgCtnBase64 = crypto.createHash('sha256');
        dataPkgCtnBase64.update(JSON.stringify(dataPkgCtn).toString("base64"));

        accountFn.Sign(dataPkgCtnBase64.digest('base64'), account.KeyPair.privKey).then(signature => {
          var datapkg = {
            c: dataPkgCtn,
            s: signature
          };
          var datapkgStr = JSON.stringify(datapkg);
          globalEvent.emit("sendMsg", datapkgStr);
          resolve(dataPkgCtn);
        });
      })
    }
  };

  var asset = {
    EncryptWithPubKey: (data, pubKey) => {
      return new Promise((resolve, reject) => {
        assetFn.EncryptWithPubKey(data, pubKey).then(result => {
          resolve(result);
        });
      })
    },
    DecryptWithPrivKey: (data, privKey) => {
      return new Promise((resolve, reject) => {
        assetFn.DecryptWithPrivKey(data, privKey).then(result => {
          resolve(result);
        });
      })
    },
    Hash: (data) => {
      return new Promise((resolve, reject) => {
        assetFn.Hash(data).then(result => {
          resolve(result);
        });
      })
    }
  };

  var app$1 = {
    AppInfo: {},
    CheckCert: () => {
      return new Promise(function (resolve, reject) {

      })
    },
    GetPolicy: () => {
      return new Promise(function (resolve, reject) {

      })
    },
    CallFn: (data, fnName, fnType) => {
      return new Promise((resolve, reject) => {
        var msgCtn = {
          ftype: fnType,
          fname: fnName,
          appID: app$1.AppInfo.appID,
          data: data
        };
        var msgCtnStr = Buffer.from(JSON.stringify(msgCtn)).toString("base64");
        var datapkg = {
          c: {
            id: crypto.randomBytes(16).toString("base64"),
            t: 3,
            c: msgCtnStr
          }
        };
        var datapkgStr = JSON.stringify(datapkg);
        globalEvent.emit("sendMsg", datapkgStr);
      })
    },
    CallPc: (data, pcName) => {
      return new Promise(function (resolve, reject) {

      })
    },
  };


  function PingNodes(nodeLinks) {
    return new Promise(function (resolve, reject) {
      var okNode = new Map();
      nodeLinks.forEach(nodeLink => {
        request({
          url: 'http://' + nodeLink + '/api/v1/ping',
          time: true
        }, (error, response, body) => {
          if (error == null) {
            if (response.statusCode == 200) {
              var node = JSON.parse(response.body);
              var nodeLinklatency = {
                link: nodeLink,
                latency: response.elapsedTime
              };
              if (okNode.has(node.result)) {
                if (okNode.get(node.result).latency > nodeLinklatency.latency) {
                  okNode.set(node.result, nodeLinklatency);
                }
              } else {
                okNode.set(node.result, nodeLinklatency);
              }
            }
          }
        });
      });
      setTimeout(() => {
        resolve(okNode);
      }, (nodeLinks.length * 5) + 300);
    })
  }

  function AppInit(nodeLinks, userAppID, appID, appVersion, eventHandler) {
    var userAppIDhex = new Buffer(userAppID, 'base64').toString('hex');
    app$1.AppInfo = {
      appID: appID
    };
    var okNode;
    PingNodes(nodeLinks).then(result => {
      okNode = result;
      if (okNode.size == 0) {
        return
      }
      okNode.forEach((node, nodeID) => {
        try {
          var connection = new WebSocket('ws://' + node.link + '/join_network_app?id=' + userAppIDhex + '&app=' + appID);
          connection.onopen = () => {
            nodeConnections.set(nodeID, connection);
            globalEvent.emit("nodeConnChange", "");
          };

          connection.onerror = (error) => {
            globalEvent.emit("error", error);
            nodeConnections.delete(nodeID, connection);
            globalEvent.emit("nodeConnChange", "");
          };

          connection.onmessage = (message) => {
            globalEvent.emit("receivedMsg", message);
          };
        } catch (error) {
          console.log(error);
        }
      });
    });
    globalEvent = new events.EventEmitter();
    globalEvent.on('nodeConnChange', () => {
      eventHandler({
        type: 2,
        data: nodeConnections.size
      });
    });
    globalEvent.on('receivedMsg', (msg) => {
      eventHandler({
        type: 1,
        data: msg.data
      });
    });
    globalEvent.on('sendMsg', (data) => {
      nodeConnections.forEach((nodeC, nodeID) => {
        nodeC.send(data);
      });
    });
    globalEvent.on('error', (error) => {
      eventHandler({
        type: 0,
        data: error
      });
    });
  }

  var main = {
    AppInit,
    account,
    asset,
    app: app$1,
    PingNodes
  };

  return main;

})));