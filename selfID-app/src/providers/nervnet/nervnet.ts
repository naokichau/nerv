import { Events, ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import nervjs from "../../assets/scripts/nervjs.js"
import blockies from "../../assets/scripts/blockies.js"

@Injectable()
export class NervnetProvider {
  account: any
  accountImg: any
  networkNodes: any;
  accountMsg: any;
  currentConnected: number = 0
  constructor(public events: Events, public toastCtrl: ToastController) {
    this.networkNodes = new Map();
    this.accountMsg = new Map();
    var nodeList = localStorage.getItem("nodelist")
    if (nodeList != undefined) {
      try {
        var nodeListParsed = JSON.parse(nodeList)
        var nodeLinkList = []
        if (nodeListParsed.length == 0) {
          setTimeout(() => {
            this.presentToast()
          }, 500);
        } else {
          nodeListParsed.forEach(node => {
            nodeLinkList.push(node[1].link)
          });
          setTimeout(() => {
            this.PingNodes(nodeLinkList)
          }, 500);
        }
      } catch (error) {
        console.log("err", JSON.stringify(error))
      }
    } else {
      setTimeout(() => {
        this.presentToast()
      }, 1000);
    }
  }

  LoadAcc() {
    return new Promise<any>((resolve) => {
      var accJson = localStorage.getItem("useracc")
      if (accJson == undefined) {
        resolve(false)
      } else {
        try {
          var accJsonParsed = JSON.parse(accJson)
          console.log(accJsonParsed)
          this.Import(accJsonParsed.privKey).then(result => {
            if (result != false) {
              var msgJson = localStorage.getItem("accountMsg")
              if (msgJson != undefined) {
                var msgList = JSON.parse(msgJson)
                this.accountMsg = new Map([...msgList])
              }
              resolve(true)
            }
            resolve(false)
          })
        } catch (error) {
          console.log("err", JSON.stringify(error))
          resolve(false)
        }
      }
    });
  }

  ConnectNetwork() {
    var nodeLinkList = []
    Array.from(this.networkNodes.entries()).forEach(node => {
      nodeLinkList.push(node[1].link)
    });
    console.log(nodeLinkList)
    nervjs.AppInit(nodeLinkList, this.account.pubKey, "", "", (nervEvent) => {
      console.log(nervEvent)
      switch (nervEvent.type) {
        case 0:
          alert(nervEvent.data)
          break;
        case 1:
          var msgParsed = JSON.parse(nervEvent.data)
          var currentList = this.accountMsg.get(msgParsed.c.s)
          if (currentList == undefined) {
            currentList = []
          }
          currentList.push({
            ctn: atob(msgParsed.c.c).replace(/"/g, ""),
            time: parseInt(msgParsed.c.st),
            id: msgParsed.c.id,
            s: msgParsed.c.s
          })
          this.accountMsg.set(msgParsed.c.s, currentList)
          break;
        case 2:
          this.currentConnected = nervEvent.data
          break;
      }
    })
  }

  CreateAcc() {
    return new Promise<any>((resolve) => {
      nervjs.account.Create().then(key => {
        localStorage.setItem("useracc", JSON.stringify(key))
        this.account = key
        this.GenAccImg()
        resolve(key)
      })
    });
  }

  SignData() {

  }
  // "LcHJdeyc9VnfcmK+2TfKrURzTprR5tNxYUbNGOh+8xg="
  Import(privKey) {
    return new Promise<any>((resolve) => {
      nervjs.account.Import(privKey).then(key => {
        localStorage.setItem("useracc", JSON.stringify(key))
        this.account = key
        this.GenAccImg()
        resolve(key)
      })
    });
  }

  GenAccImg() {
    var avatar = blockies.create({
      seed: this.account.pubKey,
      size: 8,
      scale: 9
    });
    this.accountImg = avatar.toDataURL()
  }

  PingNodes(nodeLinks) {
    return new Promise<any>((resolve) => {
      nervjs.PingNodes(nodeLinks).then(result => {
        var rt = Array.from(result.entries())
        rt.forEach(node => {
          console.log(node)
          this.networkNodes.set(node[0], node[1])
        });
        localStorage.setItem("nodelist", JSON.stringify(Array.from(this.networkNodes.entries())))
        resolve(this.networkNodes)
      })
    })
  }

  UnloadAcc() {
    localStorage.clear()
    this.account = {}
    nervjs.account.KeyPair = {}
    this.events.publish("unloadAcc")
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'No node is reachable. Please add node to be able to interact with the network!',
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'ok',
      cssClass: 'red-toast'
    });
    toast.present();
  }

  refreshNodeList() {
    var nodeLinkList = []
    Array.from(this.networkNodes.entries()).forEach(node => {
      nodeLinkList.push(node[1].link)
    });
    this.PingNodes(nodeLinkList)
  }

  SendMsgTo(receiver, msg) {
    return new Promise<any>((resolve) => {
      nervjs.account.SendMsgto(receiver, msg).then(result => {
        var currentList = this.accountMsg.get(receiver)
        if (currentList == undefined) {
          currentList = []
        }
        currentList.push({
          ctn: msg,
          time: parseInt(result.c.st),
          id: result.c.id,
          s: result.c.s
        })
        this.accountMsg.set(receiver, currentList)
        localStorage.setItem("accountMsg", JSON.stringify(Array.from(this.accountMsg.entries())))
      })
    })
  }
}
