# nerv FUNCTION LIB
import json
import sys
import time


def sendAction(action, content):
    msg = {'action': action,
           'content': json.dumps(content)}
    print json.dumps(msg)


def sendToNode(serial, data):
    content = {'serial': serial,
               'data': data}
    sendAction("NODE_SEND_DATA", content)
####################


try:
    sendAction("LOG", sys.argv[1])
    functionData = json.loads(sys.argv[1])

    def sendReply(result):
        sendToNode(functionData["serial"], result)
    deviceData = functionData["data"]

#######################
except (Exception, ArithmeticError) as e:
    template = "An exception of type {0} occurred. Arguments: {1!r}"
    message = template.format(type(e).__name__, e.args)
    sendAction("ERROR", message)
