import { Injectable } from '@angular/core';
import nervjs from "../../assets/scripts/nervjs.js"

/*
  Generated class for the NervnetProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NervnetProvider {

  constructor() {
    nervjs.Test()
  }

}
