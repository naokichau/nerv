import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CommonService } from '../service/common.service'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public tasks = [];
  public pendingTasks = []
  public doneTasks = []
  constructor(public navCtrl: NavController, public commonService: CommonService) {
    this.updateTask()
    this.commonService.task_subject.subscribe(response => {
      this.pendingTasks = this.commonService.tasks.filter(item => {
        return item.IsDone == false
      });
      this.doneTasks = this.commonService.tasks.filter(item => {
        return item.IsDone == true
      });
    })
  }
  presentAddModal() {
    this.navCtrl.push("AddPage")
  }

  checkPendingItem(id) {
    this.pendingTasks.map((task) => {
      if (task.Id == id) {
        if (task.IsDone) {
          task.IsDone = false;
        }
        else {
          task.IsDone = true;
        }
      }
    })
    this.updateTask()
  }

  checkDoneItem(id) {
    this.doneTasks.map((task) => {
      if (task.Id == id) {
        if (task.IsDone) {
          task.IsDone = false;
        }
        else {
          task.IsDone = true;
        }
      }
    })
    this.updateTask()
  }

  updateTask() {
    this.pendingTasks = this.commonService.tasks.filter(item => {
      return item.IsDone == false
    });
    this.doneTasks = this.commonService.tasks.filter(item => {
      return item.IsDone == true
    });
    localStorage.setItem("todoList", JSON.stringify(this.commonService.tasks))
  }

}
