import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonService } from '../service/common.service'

/**
 * Generated class for the AddPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {

  public tasks: any = [];
  public item: String;

  constructor(public navCtrl: NavController, public navParams: NavParams, private commonService: CommonService) {
  }

  add() {
    this.commonService.addTask(this.item);
    this.navCtrl.pop()
  }
}
