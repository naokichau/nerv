# Nerv

![Logo](nerv_small.png?raw=true "Logo")

A decentrasiled backend

![License](http://img.shields.io/badge/license-mit-blue.svg?style=for-the-badge)

IMPORTANT_NOTE:  
- Currently for DEVELOPMENT USE ONLY  
- API NOT STABLE YET  
- There are a lot of silly stuffs  
- Please install docker and run deploy.sh in deploy-env/deploy-script/dev for development environment  

GOAL: The goal of the project is to build an applicantion for people to offer their computing power / storage as a service, meaning developers don't need to spend money on a centralised hosting service instead the community whose support developer's app will host it for them and earn some money while doing it. (I haven't decide which cryptocurrency to use yet 🤔)  

Contact me via: naokichau@gmail.com

Thank you for reading this. 😘