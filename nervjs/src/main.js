import accountFn from "./lib/account";
import assetFn from "./lib/asset";
import appFn from "./lib/app";
import request from "request"
import crypto from "crypto"
import events from "events"
import utils from "./lib/utils"
import WebSocket from "ws" //nodejs
const version = 1.0
var nodeConnections = new Map()
var globalEvent
var account = {
    KeyPair: {},
    Create: () => {
        return new Promise((resolve, reject) => {
            accountFn.GenerateAcc().then(result => {
                account.KeyPair = result
                resolve(account.KeyPair)
            })
        })
    },
    Sign: (data) => {
        return new Promise((resolve, reject) => {
            accountFn.Sign(data, account.KeyPair.privKey).then(result => {
                resolve(result)
            })
        })
    },
    Verify: (data, sig, publicKey) => {
        return new Promise((resolve, reject) => {
            accountFn.Verify(data, sig, publicKey).then(result => {
                resolve(result)
            })
        })
    },
    GetPubKey: (data, sig) => {
        return new Promise((resolve, reject) => {
            accountFn.GetPubKey(data, sig).then(result => {
                resolve(result)
            })
        })
    },
    Import: (privKey) => {
        return new Promise((resolve, reject) => {
            accountFn.Import(privKey).then(result => {
                account.KeyPair = result
                resolve(account.KeyPair)
            })
        })
    },
    CreateCert: (appPolicy, expireTime) => {
        return new Promise(function (resolve, reject) {
            var preSignCert = {
                user: account.KeyPair.pubKey,
                app: appPolicy.appID,
                dataPolicy: appPolicy.dataPolicy,
                expireTime: expireTime,
                signTime: Date.now()
            }
            var preSignCertStr = JSON.stringify(preSignCert)
            assetFn.Hash(preSignCertStr).then(dataHash => {
                account.Sign(dataHash).then(result => {
                    resolve(result)
                })
            })
        })
    },
    SendMsgto: (receiverAddr, msg) => {
        return new Promise((resolve, reject) => {
            var receiverAddrHEX = new Buffer(receiverAddr, 'base64').toString('hex')
            var dataPkgCtn = {
                id: crypto.randomBytes(16).toString("hex"),
                t: 2,
                c: Buffer.from(msg).toString("base64"),
                r: receiverAddrHEX,
                s: account.KeyPair.pubKey,
                st: Date.now().toString()
            }
            const dataPkgCtnBase64 = crypto.createHash('sha256');
            dataPkgCtnBase64.update(JSON.stringify(dataPkgCtn).toString("base64"));

            accountFn.Sign(dataPkgCtnBase64.digest('base64'), account.KeyPair.privKey).then(signature => {
                var datapkg = {
                    c: dataPkgCtn,
                    s: signature
                }
                var datapkgStr = JSON.stringify(datapkg)
                globalEvent.emit("sendMsg", datapkgStr)
                resolve(dataPkgCtn)
            })
        })
    }
}

var asset = {
    EncryptWithPubKey: (data, pubKey) => {
        return new Promise((resolve, reject) => {
            assetFn.EncryptWithPubKey(data, pubKey).then(result => {
                resolve(result)
            })
        })
    },
    DecryptWithPrivKey: (data, privKey) => {
        return new Promise((resolve, reject) => {
            assetFn.DecryptWithPrivKey(data, privKey).then(result => {
                resolve(result)
            })
        })
    },
    Hash: (data) => {
        return new Promise((resolve, reject) => {
            assetFn.Hash(data).then(result => {
                resolve(result)
            })
        })
    },
    Query: (fileHash, owner) => {
        return new Promise((resolve, reject) => {
            var queryCtn = {
                assetHash: fileHash,
                owner: owner
            }
            var dataPkgCtn = {
                id: crypto.randomBytes(16).toString("hex"),
                t: 1,
                c: Buffer.from(JSON.stringify(queryCtn)).toString("base64"),
                s: account.KeyPair.pubKey,
                st: Date.now().toString()
            }
            const dataPkgCtnBase64 = crypto.createHash('sha256');
            dataPkgCtnBase64.update(JSON.stringify(dataPkgCtn).toString("base64"));

            accountFn.Sign(dataPkgCtnBase64.digest('base64'), account.KeyPair.privKey).then(signature => {
                var datapkg = {
                    c: dataPkgCtn,
                    s: signature
                }
                var datapkgStr = JSON.stringify(datapkg)
                globalEvent.emit("sendMsg", datapkgStr)
                resolve(dataPkgCtn)
            })
        })
    }
}

var app = {
    AppInfo: {},
    CheckCert: () => {
        return new Promise(function (resolve, reject) {

        })
    },
    GetPolicy: () => {
        return new Promise(function (resolve, reject) {

        })
    },
    CallFn: (data, fnName, fnType) => {
        return new Promise((resolve, reject) => {
            var base32 = require('hi-base32');
            const hash = crypto.createHash('sha256');
            hash.update(app.AppInfo.appID + fnName);
            var fnHash = base32.encode(hash.digest())
            var msgCtn = {
                ftype: fnType,
                fhash: fnHash,
                data: data
            }
            var msgCtnStr = Buffer.from(JSON.stringify(msgCtn)).toString("base64")
            var datapkg = {
                c: {
                    id: crypto.randomBytes(16).toString("base64"),
                    t: 3,
                    c: msgCtnStr,
                    s: account.KeyPair.pubKey,
                    st: Date.now().toString()
                }
            }
            var datapkgStr = JSON.stringify(datapkg)
            globalEvent.emit("sendMsg", datapkgStr)
        })
    },
    CallPc: (data, pcName) => {
        return new Promise(function (resolve, reject) {

        })
    },
}


function PingNodes(nodeLinks) {
    return new Promise(function (resolve, reject) {
        var okNode = new Map();
        nodeLinks.forEach(nodeLink => {
            request({
                url: 'http://' + nodeLink + '/api/v1/ping',
                time: true
            }, (error, response, body) => {
                if (error == null) {
                    if (response.statusCode == 200) {
                        var node = JSON.parse(response.body)
                        var nodeLinklatency = {
                            link: nodeLink,
                            latency: response.elapsedTime
                        }
                        if (okNode.has(node.result)) {
                            if (okNode.get(node.result).latency > nodeLinklatency.latency) {
                                okNode.set(node.result, nodeLinklatency)
                            }
                        } else {
                            okNode.set(node.result, nodeLinklatency)
                        }
                    }
                }
            });
        });
        setTimeout(() => {
            resolve(okNode)
        }, (nodeLinks.length * 5) + 300);
    })
}

function DropAllConnection() {
    nodeConnections.forEach((nodeC, nodeID) => {
        nodeC.close()
    });
    nodeConnections = new Map()
}

function AppInit(nodeLinks, userAppID, appID, appVersion, eventHandler) {
    var userAppIDhex = new Buffer(userAppID, 'base64').toString('hex')
    app.AppInfo = {
        appID: appID
    }
    var okNode
    PingNodes(nodeLinks).then(result => {
        okNode = result
        if (okNode.size == 0) {
            return
        }
        okNode.forEach((node, nodeID) => {
            try {
                var connection = new WebSocket('ws://' + node.link + '/join_network_app?id=' + userAppIDhex + '&app=' + appID);
                connection.onopen = () => {
                    nodeConnections.set(nodeID, connection)
                    globalEvent.emit("nodeConnChange", "")
                };

                connection.onerror = (error) => {
                    globalEvent.emit("error", error)
                    nodeConnections.delete(nodeID, connection)
                    globalEvent.emit("nodeConnChange", "")
                };

                connection.onmessage = (message) => {
                    globalEvent.emit("receivedMsg", message)
                };
            } catch (error) {
                console.log(error)
            }
        });
    })
    globalEvent = new events.EventEmitter()
    globalEvent.on('nodeConnChange', () => {
        eventHandler({
            type: 2,
            data: nodeConnections.size
        })
    })
    globalEvent.on('receivedMsg', (msg) => {
        console.log("sdkfgjioertw4")
        eventHandler({
            type: 1,
            data: msg.data
        })
    })
    globalEvent.on('sendMsg', (data) => {
        nodeConnections.forEach((nodeC, nodeID) => {
            nodeC.send(data)
        });
    })
    globalEvent.on('error', (error) => {
        eventHandler({
            type: 0,
            data: error
        })
    })
}

export default {
    AppInit,
    account,
    asset,
    app,
    PingNodes
}