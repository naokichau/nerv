import crypto from "crypto"
import secp256k1 from 'secp256k1'

function Import(privKey) {
    return new Promise(function (resolve, reject) {
        const pubKey = secp256k1.publicKeyCreate(Buffer.from(privKey, 'base64'))
        var mykey = {
            privKey: privKey,
            pubKey: pubKey.toString("base64")
        }
        resolve(mykey)
    })
}

function GenerateAcc() {
    return new Promise((resolve, reject) => {
        let privKey
        do {
            privKey = crypto.randomBytes(32)
        } while (!secp256k1.privateKeyVerify(privKey))

        // get the public key in a compressed format
        const pubKey = secp256k1.publicKeyCreate(privKey)
        var mykey = {
            privKey: privKey.toString("base64"),
            pubKey: pubKey.toString("base64")
        }
        resolve(mykey)
    })
}

function Sign(data, privKey) {
    return new Promise((resolve, reject) => {
        try {
            const sigObj = secp256k1.sign(Buffer.from(data, 'base64'), Buffer.from(privKey, 'base64'))
            resolve(sigObj.signature.toString("base64"))
        } catch (error) {
            console.log(error)
        }
    })
}

function Verify(data, signature, publicKey) {
    return new Promise((resolve, reject) => {
        resolve(secp256k1.verify(Buffer.from(data, 'base64'), Buffer.from(signature, 'base64'), Buffer.from(publicKey, 'base64')))
    })
}

// function GetPubKey(data, signature) {
//     return new Promise((resolve, reject) => {
//         var key1 = secp256k1.recover(Buffer.from(data, 'base64'), Buffer.from(signature, 'base64'), 2, false)

//         console.log(secp256k1.publicKeyVerify(key1), secp256k1.publicKeyConvert(key1, true).toString("base64"))
//     })
// }
export default {
    Sign,
    Import,
    GenerateAcc,
    Verify
}