import eccrypto from "eccrypto"
import crypto from "crypto"
import secp256k1 from 'secp256k1'

function Query(dataHash, owner) {
    return new Promise(function (resolve, reject) {

    })
}

function EncryptWithPubKey(data, pubKey) {
    return new Promise((resolve, reject) => {
        var publicKeyA = secp256k1.publicKeyConvert(Buffer.from(pubKey, "base64"), false);
        eccrypto.encrypt(publicKeyA, Buffer(data)).then(encrypted => {
            encrypted.iv = encrypted.iv.toString("base64")
            encrypted.ephemPublicKey = encrypted.ephemPublicKey.toString("base64")
            encrypted.ciphertext = encrypted.ciphertext.toString("base64")
            encrypted.mac = encrypted.mac.toString("base64")
            resolve(Buffer.from(JSON.stringify(encrypted)).toString("base64"))
        });
    })
}

function DecryptWithPrivKey(data, privKey) {
    return new Promise((resolve, reject) => {
        var encrypted = JSON.parse(Buffer.from(data, "base64"))
        encrypted.iv = Buffer.from(encrypted.iv, "base64")
        encrypted.ephemPublicKey = Buffer.from(encrypted.ephemPublicKey, "base64")
        encrypted.ciphertext = Buffer.from(encrypted.ciphertext, "base64")
        encrypted.mac = Buffer.from(encrypted.mac, "base64")
        eccrypto.decrypt(Buffer.from(privKey, 'base64'), encrypted).then(decrypted => {
            resolve(decrypted.toString())
        });
    })
}

function Hash(data) {
    return new Promise((resolve, reject) => {
        var sha256 = crypto.createHash('sha256').update(data).digest("base64");
        resolve(sha256)
    })
}
export default {
    EncryptWithPubKey,
    DecryptWithPrivKey,
    Hash
}