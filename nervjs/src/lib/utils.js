function str2ab(str) {
    var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
    var bufView = new Uint16Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

function base64ToArrayBuffer(base64) {
    var atob = require('atob');
    var binary_string = atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

function hash(params) {
    return new Promise(function (resolve, reject) {

    })
}

function toOIDArray(oid) {
    return oid.split('.').map(function (s) {
        return parseInt(s, 10)
    });
}

export default {
    str2ab,
    hash,
    toOIDArray,
    base64ToArrayBuffer
}