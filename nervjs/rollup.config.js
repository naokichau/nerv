export default {
    input: 'src/main.js',
    output: {
        name: 'nervjs',
        file: 'build/nervjs.js',
        format: 'umd',
        sourceMap: 'inline',
        globals: 'nervjs',
        external: ['randomatic', 'crypto', 'ws', 'secp256k1', 'eccrypto']
    }
};