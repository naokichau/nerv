package apprt

import (
	"errors"
	"fmt"
	"io/ioutil"

	"github.com/naokichau/nerv/nerv-node/app-runtime/function-runtime"
	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/logging"
)

func RunFunction(fnLink string, fData string, fType int) (string, error) {
	// fhash := crypto.HashSHA256([]byte(fCallData.AppID + fCallData.FName))
	// fname := fmt.Sprint(base32.StdEncoding.EncodeToString(fhash[:]))
	logging.Log("Running function " + fnLink[len(config.Data.DataDir)+7:len(fnLink)] + " with data: " + fData)
	switch fType {
	case 0: // js
		dat, err := ioutil.ReadFile(fnLink)
		if err != nil {
			fmt.Println(err)
			return "", errors.New("Error when reading function. Detail: " + err.Error())
		}
		output, err := functionrt.JSExec(string(dat), fData)
		if err != nil {
			return "", errors.New("Error when executing function. Detail: " + err.Error())
		}
		return output.Result, nil
	case 1: // binary
		return "", errors.New("Not supported yet")
	case 2: // container
		return "", errors.New("Not supported yet")
	default:
		return "", errors.New("Unknown function type")
	}
}

// func RunProcedure(data types.AppDataPkg, procedure string) error {
// 	var procedureOutput [][][]string
// 	stages := strings.Split(procedureStages, ",")
// 	for stageIdx, stage := range stages {
// 		functionGroups := strings.Split(stage, "|")
// 		for functionGroupIdx, functionGroup := range functionGroups {
// 			go func() {
// 				functions := strings.Split(functionGroup, ":")
// 				for functionIdx, function := range functions {
// 					go func() {
// 						_, err := url.ParseRequestURI(function)
// 						if err != nil {
// 							if function == "SENDBACK" {

// 							} else {
// 								if strings.LastIndex(function, "SENDTO") == 0 {
// 									var re = regexp.MustCompile(`SENDTO\((.*)\)`)
// 									queryContext := re.FindStringSubmatch(function)[1]
// 									queryParams := strings.Split(queryContext, ";")
// 									for _, queryParam := range queryParams {
// 										fmt.Println(queryParam)
// 									}
// 								} else {
// 									output, err := RunFunction(data, function)
// 									if err != nil {
// 										output = err.Error()
// 									}
// 									procedureOutput[stageIdx][functionGroupIdx][functionIdx] = output
// 								}
// 							}
// 						} else {
// 							//Forward data
// 						}
// 					}()
// 				}
// 			}()
// 		}
// 	}
// 	return nil
// }

// func flow(data string, pipe string) (string, error) {
// 	var flowResult string
// 	var err error
// 	fData := []string{data}
// 	fmt.Println(fData)
// 	// switch pipeStage.Do.Action {
// 	// case "EXEC_FUNC":
// 	// 	flowResult, err = function.Exec(pipeStage.Do.Content, fData)
// 	// 	break
// 	// // case "NODE_SEND_DATA":
// 	// // 	go new(senders.Device).SendData("DATA", outData.Content)
// 	// // 	break
// 	// // case "NODE_SEND_CONFIG":
// 	// // 	if err := new(senders.Device).SendData("CONFIG", outData.Content); err != nil {
// 	// // 		logging.Error(404, err.Error())
// 	// // 	}
// 	// // 	break
// 	// default:
// 	// 	return "", errors.New("Unknown action type")
// 	// }
// 	if err != nil {
// 		return "", err
// 	}
// 	return flowResult, nil
// }

// func ProcessData(data types.FunctionDataPkg) error {
// 	// tags := strings.Split(data.Tags, ",")
// 	// alreadyRunPipe := make(map[string]bool)
// 	// for _, tag := range tags {
// 	// 	fmt.Println(tag)
// 	// 	// pipelines, err := redisDB.Cache.SMembers(tag).Result()
// 	// 	// if err != nil {
// 	// 	// 	return err
// 	// 	// }
// 	// 	// if len(pipelines) == 0 {
// 	// 	// }
// 	// 	// for _, pipeline := range pipelines {
// 	// 	// 	pipelineStages := strings.Split(pipeline, " ")[1]
// 	// 	// 	RunPipe(string(data.Data), pipelineStages)
// 	// 	// 	//func = hash(namespace+funcName)
// 	// 	// }
// 	// 	return nil
// 	// }
// 	return nil
// }
