package functionrt

import (
	"bufio"
	"bytes"
	"strings"
)

type OutputData struct {
	Status  string
	Result  string
	LogData []string
}

func readOutput(out []byte) (OutputData, error) {
	var output OutputData
	in := bufio.NewScanner(bytes.NewReader(out))
	for in.Scan() {
		outLine := strings.Split(in.Text(), " | ")
		switch outLine[0] {
		case "LOG":
			output.LogData = append(output.LogData, outLine[1])
		case "OUTPUT":
			output.Status = "OK"
			output.Result = outLine[1]
			return output, nil
		}
	}
	if err := in.Err(); err != nil {
		return output, err
	}
	return output, nil
}

func readJSOutput(out string) (OutputData, error) {
	var output OutputData
	return output, nil
}
