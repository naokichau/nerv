package functionrt

import (
	"errors"
	"fmt"
	"os/exec"
	"time"

	"github.com/naokichau/nerv/nerv-node/constvar"

	"github.com/naokichau/nerv/nerv-node/logging"
)

func ContainerBootstrap(imageName string) error {
	logging.Log("Loading function " + imageName)
	cmd := exec.Command("singularity", "instance.start", "docker://naokichau/test1", "test1")
	if err := cmd.Start(); err != nil {
		logging.Error(constvar.ErrExecFunc, "Can't load function", err)
		return err
	}
	return nil
}

// Exec create a container to run function
func ContainerExec(funcID string, data []string) (OutputData, error) {
	logging.Log("Running function " + funcID)
	err := BinaryBootstrap(funcID)
	if err != nil {
		return OutputData{}, errors.New("Failed to bootstrap function " + funcID)
	}
	fData := append([]string{"exec", "-c", "-e", "-p", "instance://test1", "/run.sh"}, data...)
	cmd := exec.Command("singularity", fData...)
	start := time.Now()
	stdout, err := cmd.Output()
	if err != nil {
		logging.Error(constvar.ErrExecFunc, "Can't exec function", err)
		return OutputData{}, err
	}
	_ = cmd.Wait()
	elapsed := time.Since(start)
	logging.Log("Function exit. Elapsed time: " + fmt.Sprint(elapsed))
	fOutput, err := readOutput(stdout)
	if err != nil {
		return OutputData{}, err
	}
	return fOutput, nil
}
