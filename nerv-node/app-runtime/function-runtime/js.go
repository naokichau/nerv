package functionrt

import (
	"fmt"
	"time"

	"github.com/naokichau/nerv/nerv-node/logging"
	"github.com/robertkrimen/otto"
)

func JSExec(funcScript string, data string) (OutputData, error) {
	vm := otto.New()
	start := time.Now()
	vm.Set("input", data)
	output, err := vm.Run(funcScript)
	if err != nil {
		fmt.Println("JS error:", err)
	}
	elapsed := time.Since(start)
	go logging.Log("Function exit. Elapsed time: " + fmt.Sprint(elapsed))
	fOutput, err := readOutput([]byte(output.String()))
	if err != nil {
		return OutputData{}, err
	}
	return fOutput, nil
}
