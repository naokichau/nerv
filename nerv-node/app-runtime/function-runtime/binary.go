package functionrt

import (
	"errors"
	"fmt"
	"os/exec"
	"time"

	"github.com/naokichau/nerv/nerv-node/constvar"

	"github.com/naokichau/nerv/nerv-node/logging"
)

func BinaryBootstrap(funcID string) error {
	return nil
}

func BinaryExec(funcID string, data string) (OutputData, error) {
	logging.Log("Running function " + funcID)
	err := BinaryBootstrap(funcID)
	if err != nil {
		return OutputData{}, errors.New("Failed to bootstrap function " + funcID)
	}
	cmd := exec.Command("/functions/binary/"+funcID, data)
	start := time.Now()
	stdout, err := cmd.Output()
	if err != nil {
		logging.Error(constvar.ErrExecFunc, "Can't exec function", err)
		return OutputData{}, err
	}
	_ = cmd.Wait()
	elapsed := time.Since(start)
	logging.Log("Function exit. Elapsed time: " + fmt.Sprint(elapsed))
	fOutput, err := readOutput(stdout)
	if err != nil {
		return OutputData{}, err
	}
	return fOutput, nil
}
