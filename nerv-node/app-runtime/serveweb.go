package apprt

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/constvar"
	"github.com/naokichau/nerv/nerv-node/database/storage"
)

func ServeWeb(params []string, r *http.Request, w http.ResponseWriter) {
	appAddress := params[1]
	_, err := storage.CheckExist(config.Data.DataDir + "/web/" + appAddress)
	if err != nil {
		extractWeb(params[1])
		notFoundPage, _ := base64.StdEncoding.DecodeString(constvar.NotFoundPage)
		w.Write([]byte(notFoundPage))
		return
	}
	switch params[2] {
	case "main":
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		dat, err := ioutil.ReadFile(config.Data.DataDir + "/web/" + appAddress + "/index.html")
		if err != nil {
			fmt.Println(err)
			return
		}
		w.Write(dat)
		return
	default:
		file := strings.Split(params[0], "?")
		dat, err := ioutil.ReadFile(config.Data.DataDir + file[0])
		if err != nil {
			w.Write([]byte("This file doesn't exist"))
			return
		}
		fileExtension := strings.Split(file[0], ".")
		contentType := mime.TypeByExtension("." + fileExtension[len(fileExtension)-1])
		w.Header().Set("Content-Type", contentType)
		w.Write(dat)
		return
	}
}

func extractWeb(appAddress string) {
	matches, err := filepath.Glob(config.Data.DataDir + "/storage/1" + appAddress + "/web.zip")
	if err != nil {
		fmt.Println(err)
		return
	}
	if len(matches) != 0 {
		fpath := config.Data.DataDir + "/web/" + appAddress
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			fmt.Println(err)
			return
		}
		_, err = storage.Unzip(matches[0], fpath)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}
