package sysresource

import (
	"fmt"
	"os"
	"sync"
	"syscall"

	stats "gopkg.in/go-playground/stats.v1"
)

type VirtualSpace struct {
	Capacity int
	Used     int
}

type SysInfo struct {
	stats *stats.Stats
	sync.Mutex
}

func (sys *SysInfo) Init() {
	sys.stats = &stats.Stats{}
}
func (sys *SysInfo) GetDiskSpace() (VirtualSpace, error) {
	var disk VirtualSpace
	var stat syscall.Statfs_t

	wd, err := os.Getwd()
	if err != nil {
		return disk, err
	}
	syscall.Statfs(wd, &stat)

	// Available blocks * size per block = available space in bytes
	fmt.Println(stat.Bavail * uint64(stat.Bsize))
	return disk, nil
}

func (sys *SysInfo) GetOSInfo() error {
	return nil
}

func (sys *SysInfo) GetCPUInfo() error {
	return nil
}

func (sys *SysInfo) GetRamInfo() (stats.MemInfo, stats.GoMemory, error) {
	sys.stats.GetMemoryInfo(true, true)
	fmt.Println(sys.stats.GoInfo)
	return nil
}

// func GetRam() (VirtualSpace, error) {
// 	var ram VirtualSpace
// 	var m runtime.MemStats
// 	runtime.ReadMemStats(&m)
// 	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
// 	fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
// 	fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
// 	fmt.Printf("\tNumGC = %v\n", m.NumGC)

// 	return ram, nil
// }

// func bToMb(b uint64) uint64 {
// 	return b / 1024 / 1024
// }
