package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"

	"github.com/naokichau/nerv/nerv-node/controller"
	"github.com/naokichau/nerv/nerv-node/eventch"

	"github.com/naokichau/nerv/nerv-node/constvar"
	"github.com/naokichau/nerv/nerv-node/database/cachedb"

	"github.com/fatih/color"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/database/storage"
	"github.com/naokichau/nerv/nerv-node/logging"
	"github.com/naokichau/nerv/nerv-node/nervnet"
)

func initNode() {
	storage.InitStorage(config.Data.DataDir)
	cachedb.Create()
	ctrl := &controller.Controller{
		Params: controller.Params{
			DataDir: config.Data.DataDir,
			NodeID:  config.HostID,
		},
	}
	node := &nervnet.Nervnet{
		Params: nervnet.Params{
			NodeID:     config.HostID,
			InitNodes:  config.Data.InitNodes,
			Controller: ctrl,
		},
	}
	eventCh := &eventch.EventChannel{
		Params: eventch.Params{
			NodeID:  config.HostID,
			Nervnet: node,
		},
	}

	ctrl.Init()
	eventCh.Init()
	node.Init()
}

func main() {
	// 	print(` _   _ ______ _______      __
	// | \ | |  ____|  __ \ \    / /
	// |  \| | |__  | |__) \ \  / /
	// | .   |  __| |  _  / \ \/ /
	// | |\  | |____| | \ \  \  /
	// |_| \_|______|_|  \_\  \/
	// `)

	configFilePtr := flag.String("config", "./config.yaml", "path to config file")
	flag.Parse()

	c := color.New(color.FgBlue, color.Bold)
	println(c.Sprint("[nerv-node]"), "["+constvar.NodeVersion()+"]", "\n")
	k := make(chan os.Signal, 1)
	signal.Notify(k, os.Interrupt)
	go func() {
		for sig := range k {
			if sig.String() == "interrupt" {
				fmt.Println("\nStopping nerv-node...")
				os.Exit(0)
			}
		}
	}()
	err := config.ReadConfig(*configFilePtr)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("Node info: \nNodeID: %s \nBroadcast IPs: %s \n\n", config.HostID, config.Network.IPs)
	if config.Data.DebugMode {
		fmt.Println("Starting node in TEST mode")
	} else {
		fmt.Println("Starting node in SECURE mode")
	}
	logging.DataDir = config.Data.DataDir
	if err := checkSystem(config.Data.NodeType); err != nil {
		logging.Fatal(constvar.ErrConn, "System doesn't satify requirement to run in "+config.Data.NodeType, err)
	}
	initNode()
}

func checkSystem(mode string) error {
	switch mode {
	case "cloud":
	case "edge":
	}
	return nil
}
