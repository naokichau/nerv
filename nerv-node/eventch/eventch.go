package eventch

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/database/cachedb"
	"github.com/naokichau/nerv/nerv-node/database/storage"

	"github.com/naokichau/nerv/nerv-node/app-runtime"
	"github.com/naokichau/nerv/nerv-node/constvar"
	"github.com/naokichau/nerv/nerv-node/logging"
	"github.com/naokichau/nerv/nerv-node/nervnet"
	"github.com/naokichau/nerv/nerv-node/types"
)

// var (
// 	CallFnCh              chan types.FunctionCall
// 	ReceivedMsgList       nervnet.MsgList
// 	WaitForRepsondMsgList nervnet.WaitForRepsondMsgList
// )

type EventChannel struct {
	CallFnCh              chan types.FunctionCall
	ReceivedMsgList       nervnet.MsgList
	WaitForRepsondMsgList nervnet.WaitForRepsondMsgList
	Params                Params
}

type Params struct {
	NodeID     string
	Controller interface {
		IncreaseDemand(demandCtx interface{}, demandType int)
	}
	Nervnet *nervnet.Nervnet
}

func (eventCh *EventChannel) Init() {
	eventCh.CallFnCh = make(chan types.FunctionCall)
	eventCh.WaitForRepsondMsgList = nervnet.WaitForRepsondMsgList{List: make(map[string]*nervnet.RequestMsg), Respond: make(chan nervnet.RespondMsg)}
	eventCh.ReceivedMsgList = nervnet.MsgList{List: make(map[string]*types.DataPkg)}
	go eventCh.startFnRuntimeCh()
	go eventCh.startMsgProcessing()
}

////////////////////////////////////////////////////
// WARNING!!! THE CODE BELOW IS VERY CONFUSING 😑 //
///////////////////////////////////////////////////

func (eventCh *EventChannel) startFnRuntimeCh() {
	logging.Log("Starting app runtime channel...")
	for {
		callFnData := <-eventCh.CallFnCh
		// should check for number of running function
		go func() {
			result, err := apprt.RunFunction(callFnData.FnData.FHash, callFnData.FnData.Data, callFnData.FnData.Ftype)
			if err != nil {
				logging.Error(constvar.ErrExecFunc, "Function execution error.", err)
				resultByte, _ := json.Marshal(err)
				eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
					Msg: types.DataPkg{
						Data: types.MsgContent{
							ID:         callFnData.MsgID,
							Content:    resultByte,
							Status:     false,
							ActionType: 6,
							SendTime:   time.Now().String(),
							Sender:     eventCh.Params.NodeID,
						},
					},
					Sender: eventCh.Params.NodeID,
				}
				return
			}
			resultByte, _ := json.Marshal(result)
			eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
				Msg: types.DataPkg{
					Data: types.MsgContent{
						ID:         callFnData.MsgID,
						Content:    resultByte,
						Status:     true,
						ActionType: 6,
						SendTime:   time.Now().String(),
						Sender:     eventCh.Params.NodeID,
					},
				},
				Sender: eventCh.Params.NodeID,
			}
		}()
	}
}

func (eventCh *EventChannel) startMsgProcessing() {
	logging.Log("Starting messaging channels...")
	go eventCh.respondMsgHandler()
	for {
		msg := <-eventCh.Params.Nervnet.NodeMsgCh
		eventCh.ReceivedMsgList.Lock()
		if _, ok := eventCh.ReceivedMsgList.List[msg.MsgData.Data.ID]; !ok || msg.MsgData.Data.ActionType == 6 {
			eventCh.ReceivedMsgList.List[msg.MsgData.Data.ID] = &msg.MsgData
			eventCh.ReceivedMsgList.Unlock()
			switch msg.MsgData.Data.ActionType {
			case 0: // node event
				go eventCh.Params.Nervnet.ProcessEvent(&msg.MsgData.Data.Content)
			case 1: // query
				eventCh.addToRequestWaitList(msg.MsgData.Data.ID, nervnet.RequestMsg{
					RequestorType: msg.SenderType,
					Requestor:     msg.Sender,
					RequestType:   0})
				// AssetHash is a base32 string
				var queryInfo types.QueryData
				err := json.Unmarshal(msg.MsgData.Data.Content, &queryInfo)
				if err != nil {
					fmt.Println(err)
					eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
						Msg: types.DataPkg{
							Data: types.MsgContent{
								ID:         msg.MsgData.Data.ID,
								ActionType: 6,
								Status:     false,
								Content:    []byte("Invalid query format"),
								SendTime:   time.Now().String(),
								Sender:     eventCh.Params.NodeID,
							},
						},
						Sender: eventCh.Params.NodeID,
					}
					continue
				}
				eventCh.WaitForRepsondMsgList.List[msg.MsgData.Data.ID].RequestCtx = &queryInfo
				//
				// Should check from database instead
				//
				links, err := storage.CheckExist(config.Data.DataDir + "/files/4" + queryInfo.AssetHash + "*")
				if err != nil {
					eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
						Msg: types.DataPkg{
							Data: types.MsgContent{
								ID:         msg.MsgData.Data.ID,
								ActionType: 6,
								Status:     false,
								Content:    []byte("Error while finding file. Detail: " + err.Error()),
								SendTime:   time.Now().String(),
								Sender:     eventCh.Params.NodeID,
							},
						},
						Sender: eventCh.Params.NodeID,
					}
					continue
				}
				if len(links) == 0 {
					eventCh.Params.Nervnet.BroadcastMsg(msg.MsgData, msg.Sender)
					continue
				}
				for i, link := range links {
					dirLink := strings.Split(link, "/")
					match := CheckFileSignature(dirLink[len(dirLink)-1], queryInfo.Owner)
					if !match {
						if i == len(links) {
							eventCh.Params.Nervnet.BroadcastMsg(msg.MsgData, msg.Sender)
						}
					} else {
						f, _ := os.Open(link)
						stat, _ := f.Stat()
						var assetLink types.AssetLink
						assetLink.NodeNetInfo = eventCh.Params.Nervnet.NodeList.Nodes[eventCh.Params.NodeID].Net
						assetLink.NodeID = eventCh.Params.NodeID
						assetLink.Link = dirLink[len(dirLink)-1][1:len(dirLink[len(dirLink)-1])]
						assetLink.Size = stat.Size()
						assetLinkBytes, _ := json.Marshal(assetLink)
						eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
							Msg: types.DataPkg{
								Data: types.MsgContent{
									ID:         msg.MsgData.Data.ID,
									ActionType: 6,
									Status:     true,
									Content:    assetLinkBytes,
									SendTime:   time.Now().String(),
									Sender:     eventCh.Params.NodeID,
								},
							},
							Sender: eventCh.Params.NodeID,
						}
					}
				}
			case 2: // send msg to
				if ok := eventCh.Params.Nervnet.ConnectedUser.CheckExist(msg.MsgData.Data.Receiver); ok {
					eventCh.Params.Nervnet.ConnectedUser.List[msg.MsgData.Data.Receiver].MsgCh <- msg.MsgData
				} else {
					eventCh.Params.Nervnet.BroadcastMsg(msg.MsgData, "")
					cachedb.UserMsgCache.Set(msg.MsgData.Data.Receiver, msg.MsgData, 60, 2)
				}
			case 3: // function call
				eventCh.addToRequestWaitList(msg.MsgData.Data.ID, nervnet.RequestMsg{
					RequestorType: msg.SenderType,
					Requestor:     msg.Sender,
					RequestType:   1})
				fCallData := types.FunctionCall{
					MsgID: msg.MsgData.Data.ID,
				}
				err := json.Unmarshal(msg.MsgData.Data.Content, &fCallData.FnData)
				if err != nil {
					fmt.Println(err)
					eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
						Msg: types.DataPkg{
							Data: types.MsgContent{
								ID:         msg.MsgData.Data.ID,
								ActionType: 6,
								Status:     false,
								Content:    []byte("Invalid function call format"),
								SendTime:   time.Now().String(),
								Sender:     eventCh.Params.NodeID,
							},
						},
						Sender: eventCh.Params.NodeID,
					}
					continue
				}
				eventCh.WaitForRepsondMsgList.List[msg.MsgData.Data.ID].RequestCtx = &types.FunctionData{Ftype: fCallData.FnData.Ftype, FHash: fCallData.FnData.FHash}
				fType := "*"
				if fCallData.FnData.Ftype != -1 {
					if fCallData.FnData.Ftype != 0 && fCallData.FnData.Ftype != 1 && fCallData.FnData.Ftype != 2 {
						eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
							Msg: types.DataPkg{
								Data: types.MsgContent{
									ID:         msg.MsgData.Data.ID,
									ActionType: 6,
									Status:     false,
									Content:    []byte("Invalid function call format"),
									SendTime:   time.Now().String(),
									Sender:     eventCh.Params.NodeID,
								},
							},
							Sender: eventCh.Params.NodeID,
						}
						continue
					}
					fType = strconv.Itoa(fCallData.FnData.Ftype)
				}
				fmt.Println(config.Data.DataDir)
				fnFile, err := storage.CheckExist(config.Data.DataDir + "/files/1" + fType + fCallData.FnData.FHash)
				if err != nil {
					fmt.Println(err)
					eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
						Msg: types.DataPkg{
							Data: types.MsgContent{
								ID:         msg.MsgData.Data.ID,
								ActionType: 6,
								Status:     false,
								Content:    []byte("Invalid function call format"),
								SendTime:   time.Now().String(),
								Sender:     eventCh.Params.NodeID,
							},
						},
						Sender: eventCh.Params.NodeID,
					}
					continue
				}
				if len(fnFile) == 0 {
					eventCh.Params.Nervnet.BroadcastMsg(msg.MsgData, msg.Sender)
					continue
				}
				fCallData.FnData.FHash = fnFile[0] // will take the 1st one for now, in the future app controller will do a benchmark on each func type to choose the onre suitable for the node
				eventCh.CallFnCh <- fCallData
			case 4: // procedure call
				eventCh.addToRequestWaitList(msg.MsgData.Data.ID, nervnet.RequestMsg{
					RequestorType: msg.SenderType,
					Requestor:     msg.Sender,
					RequestType:   1})
			case 5: // service call
				eventCh.addToRequestWaitList(msg.MsgData.Data.ID, nervnet.RequestMsg{
					RequestorType: msg.SenderType,
					Requestor:     msg.Sender,
					RequestType:   1})
			case 6: // respond msg for other node
				eventCh.WaitForRepsondMsgList.Respond <- nervnet.RespondMsg{
					Msg:    msg.MsgData,
					Sender: msg.Sender,
				}
			}
		} else {
			//????
			eventCh.ReceivedMsgList.Unlock()
		}
	}
}

func (eventCh *EventChannel) respondMsgHandler() {
	for {
		respond := <-eventCh.WaitForRepsondMsgList.Respond
		eventCh.WaitForRepsondMsgList.Lock()
		if reqMsg, ok := eventCh.WaitForRepsondMsgList.List[respond.Msg.Data.ID]; ok {
			switch reqMsg.RequestorType {
			case 0:
				if ok := eventCh.Params.Nervnet.ConnectedNodes.CheckExist(reqMsg.Requestor); ok {
					eventCh.Params.Nervnet.SendMsgToNode(reqMsg.Requestor, &respond.Msg)
				} else {
					eventCh.Params.Nervnet.BroadcastMsg(respond.Msg, respond.Sender)
				}
			case 1:
				if ok := eventCh.Params.Nervnet.ConnectedUser.CheckExist(reqMsg.Requestor); ok {
					eventCh.Params.Nervnet.ConnectedUser.List[reqMsg.Requestor].MsgCh <- respond.Msg
					if respond.Msg.Data.Status {
						// increase demand if request is fullfil
						var demandCtx interface{}
						if reqMsg.RequestType == 0 {
							var assetLink types.AssetLink
							_ = json.Unmarshal(respond.Msg.Data.Content, &assetLink)
							var assetCtx struct {
								AssetLink types.AssetLink
								Owner     string
							}
							assetCtx.AssetLink = assetLink
							assetCtx.Owner = reqMsg.RequestCtx.(types.QueryData).Owner
							demandCtx = assetCtx
						} else {
							var fnCtx struct {
								FHash string
								Ftype int
								AppID string
							}
							fnCtx.FHash = reqMsg.RequestCtx.(*types.FunctionData).FHash
							fnCtx.Ftype = reqMsg.RequestCtx.(*types.FunctionData).Ftype
							fnCtx.AppID = eventCh.Params.Nervnet.ConnectedUser.List[reqMsg.Requestor].AppID
							demandCtx = fnCtx
						}
						go eventCh.Params.Controller.IncreaseDemand(demandCtx, reqMsg.RequestType)
					}
				} else {
					eventCh.Params.Nervnet.BroadcastMsg(respond.Msg, respond.Sender)
				}
			}
			delete(eventCh.WaitForRepsondMsgList.List, respond.Msg.Data.ID)
			eventCh.WaitForRepsondMsgList.Unlock()
		} else {
			//????
			eventCh.WaitForRepsondMsgList.Unlock()
		}
	}
}

func CheckFileSignature(filename string, owner string) bool {
	return true
}

func (eventCh *EventChannel) addToRequestWaitList(msgID string, request nervnet.RequestMsg) {
	eventCh.WaitForRepsondMsgList.Lock()
	eventCh.WaitForRepsondMsgList.List[msgID] = &request
	eventCh.WaitForRepsondMsgList.Unlock()
}
