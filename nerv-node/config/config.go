package config

import (
	"net"

	"github.com/naokichau/nerv/nerv-node/database/storage"
	"github.com/naokichau/nerv/nerv-node/nervnet/netutils"
	"github.com/naokichau/nerv/nerv-node/types"
	uuid "github.com/satori/go.uuid"
	"gopkg.in/yaml.v2"
)

type cFile struct {
	DataDir      string    `yaml:"datadir"`
	NodePort     int       `yaml:"node-port"`
	NodeType     string    `yaml:"node-type"`
	DebugMode    bool      `yaml:"debug-mode"`
	NodeIdentity string    `yaml:"node-identity"`
	NetworkID    int       `yaml:"network-id"`
	BroadcastIPs []string  `yaml:"broadcast-ips"`
	InitNodes    []string  `yaml:"init-nodes"`
	Policy       PolicyCfg `yaml:"policy"`
	LogCfg       LogCfg    `yaml:"log-config"`
	Owner        string    `yaml:"owner"`
}

type PolicyCfg struct {
	MinPeer   int          `yaml:"min-peer"`
	MaxPeer   int          `yaml:"max-peer"`
	Retriever RetrieverCfg `yaml:"retriever"`
	Uploader  UploaderCfg  `yaml:"uploader"`
}
type RetrieverCfg struct {
	MaxConnPerFile     int `yaml:"max-conn-per-file"`
	MaxActiveRetriever int `yaml:"max-active-retriever"`
}
type UploaderCfg struct {
	MaxActiveUploader int `yaml:"max-active-uploader"`
}

type LogCfg struct {
	LogTypes   []string `yaml:"log-types"`
	LogModules []string `yaml:"log-modules"`
	LogOutput  string   `yaml:"log-output"`
}

var HostID string = uuid.Must(uuid.NewV4()).String()
var Network types.NodeNetworkInfo
var Data cFile

func ReadConfig(file string) error {
	fileCtn, err := storage.ReadfileFromSource(file)
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(fileCtn, &Data)
	if err != nil {
		return err
	}
	if Data.NodeIdentity != "" {
		HostID = uuid.NewV3(uuid.NamespaceX500, Data.NodeIdentity).String()
	}
	Network = types.NodeNetworkInfo{IPs: getBroadcastableIPs(), NodePort: Data.NodePort}
	return nil
}

func getBroadcastableIPs() []net.IP {
	var broadcastAddrs []net.IP
	if len(Data.BroadcastIPs) == 0 {
		broadcastAddrs = netutils.GetAllInterfaceIPs()
	} else {
		for _, addr := range Data.BroadcastIPs {
			broadcastAddrs = append(broadcastAddrs, net.ParseIP(addr))
		}
	}
	return broadcastAddrs
}
