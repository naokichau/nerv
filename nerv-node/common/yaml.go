package common

type YamlApp struct {
	Version     string            `yaml:"version"`
	Address     string            `yaml:"Address"`
	Name        string            `yaml:"Name"`
	Description string            `yaml:"Description"`
	AppVersion  string            `yaml:"AppVersion"`
	Checksums   map[string]string `yaml:"Checksums"`
}

type YamlService struct {
	Version int `yaml:"version"`
}
type YamlAppPolicy struct {
	Version int `yaml:"version"`
}

type YamlProcedure struct {
	Version int `yaml:"version"`
}

// type YamlNamespace struct {
// 	Version         string   `yaml:"version"`
// 	Description     string   `yaml:"description"`
// 	AssetModelsHash string   `yaml:"asset_models_hash"`
// 	ProceduresHash  string   `yaml:"procedures_hash"`
// 	ContractsHash   string   `yaml:"contracts_hash"`
// 	NotifyOptions   []string `yaml:"notify_options"`
// }
// type YamlProcedure struct {
// 	Description string   `yaml:"description"`
// 	Type        string   `yaml:"type"`
// 	Hash        []string `yaml:"hash"`
// }
// type YamlContracts struct {
// 	Description  string   `yaml:"description"`
// 	ProcessTags  []string `yaml:"process_tags"`
// 	Stages       []string `yaml:"stages"`
// 	SupportArchs []string `yaml:"support_architectures"`
// }
// type YamlAsset struct {
// 	Serial string   `yaml:"serial"`
// 	Config []string `yaml:"config"`
// }
// type YamlDeviceModel struct {
// 	Description          string   `yaml:"description"`
// 	Type                 string   `yaml:"type"`
// 	StatusUpdateInterval int      `yaml:"status_update_interval"`
// 	BuildService         string   `yaml:"build_service"`
// 	FirmwareHash         string   `yaml:"firmware_hash"`
// 	DefaultVariables     []string `yaml:"default_variables"`
// 	EnrollPolicy         []string `yaml:"enroll_policy"`
// 	ValidatorEndPoints   []string `yaml:"validator_endpoints"`
// }
