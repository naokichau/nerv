package common

import (
	"archive/zip"
	"encoding/json"
	"net"
	"time"
)

type App struct {
	Address     string
	Name        string
	Description string
	Version     string
	Checksums   map[string]string
	Signature   string
	UpdatedAt   time.Time
}

type AppPkg struct {
	Info          YamlApp
	Procedures    YamlProcedure
	Services      YamlService
	Frontend      *zip.File
	FunctionFiles map[string]*zip.File
	AssetFiles    map[string]*zip.File
}

type FunctionOutput struct {
	Status string `json:"status"`
	Data   string `json:"data"`
}

type FunctionData struct {
	Ftype   int    `json:"ftype"`
	FHash   string `json:"fhash"`
	Timeout int    `json:"timeout"`
	AppID   string `json:"appID"`
	Data    string `json:"data"`
}

type FunctionCall struct {
	FnData FunctionData
	MsgID  string
}
type QueryData struct {
	AssetHash string `json:"assetHash"`
	Owner     string `json:"owner"`
}

type DataPkg struct {
	Data         MsgContent `json:"c"`
	Signature    string     `json:"s"`
	DesireRoutes []string   `json:"dr"`
	ActualRoutes []string   `json:"ar"`
}

type MsgContent struct {
	ID         string `json:"id"`
	Status     bool   `json:"stt"`
	ActionType int    `json:"t"`
	Content    []byte `json:"c"`
	SendTime   string `json:"st"`
	Receiver   string `json:"r"`
	Sender     string `json:"s"`
	Private    bool   `json:"p"`
}

type AssetLink struct {
	NodeNetInfo NodeNetworkInfo
	NodeID      string
	Link        string
	Size        int64
}

type NodeEvent struct {
	Type    int
	Content []byte
}
type NodeNetworkInfo struct {
	IPs      []net.IP
	NodePort int
}

type NodeLatency struct {
	NodeID     string
	IP         string
	AvgLatency float64
	AvgDev     float64
}

type NodeResource struct {
	Memory        int
	DiskAvailable int
	HashRate      int
}

type APIRespond struct {
	Result interface{} `json:"result"`
}

func (data APIRespond) Marshal() ([]byte, error) {
	res, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	return res, err
}

// type Asset struct {
// 	Serial      string    `json:"serial"`
// 	IsActive    bool      `json:"is_active"`
// 	Namespace   string    `json:"namespace"`
// 	Model       string    `json:"model"`
// 	CreatedTime time.Time `json:"create_time"`
// 	UpdatedTime time.Time `json:"updated_time"`
// }

// type AssetState struct {
// 	Serial      string            `json:"serial"`
// 	State       map[string]string `json:"state"`
// 	UpdatedTime time.Time         `json:"updated_time"`
// }

// type AssetModel struct {
// 	Name               string    `json:"name"`
// 	Description        string    `json:"description"`
// 	Type               string    `json:"type"`
// 	Tags               string    `json:"tags"`
// 	EnrollPolicy       string    `json:"enroll_policy"`
// 	ValidatorEndPoints string    `json:"validator_endpoints"`
// 	CreatedTime        time.Time `json:"create_time"`
// 	UpdatedTime        time.Time `json:"updated_time"`
// }

// type Contract struct {
// 	Name        string    `json:"name"`
// 	Description string    `json:"description"`
// 	ProcessTags string    `json:"process_tags"`
// 	Stages      string    `json:"stages"`
// 	CreatedTime time.Time `json:"create_time"`
// 	UpdatedTime time.Time `json:"updated_time"`
// }

// type ContractStage struct {
// 	Description   string `json:"description"`
// 	Procedure     string `json:"procedure"`
// 	ProcedureType string `json:"procedure_type"`
// 	Timeout       int    `json:"timeout"`
// }

// type Appfile struct {
// 	Frontend      *zip.File
// 	FunctionFiles map[string]*zip.File
// 	MediaFiles    map[string]*zip.File
// }

// type Token struct {
// 	Token  []byte    `json:"token"`
// 	Type   int       `json:"type"`
// 	Issuer string    `json:"issuer"`
// 	Expire time.Time `json:"expire"`
// }
