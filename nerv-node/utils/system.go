package utils

import (
	"bytes"
	"io"
	"os"
	"path/filepath"

	"github.com/naokichau/nerv/nerv-node/types"
)

func WriteFiletoDisk(filename string, fileDir string, fileData []byte, fmode os.FileMode) error {
	if err := os.MkdirAll(filepath.Dir(fileDir), os.ModePerm); err != nil {
		return err
	}
	// fmt.Println("Writing", fileDir, filename)
	outFile, err := os.OpenFile(fileDir+filename, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, fmode)
	if err != nil {
		return err
	}
	_, err = io.Copy(outFile, bytes.NewBuffer(fileData))
	if err != nil {
		return err
	}
	return nil
}

func GetNodeResource() types.NodeResource {
	var resource types.NodeResource
	return resource
}
