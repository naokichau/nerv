package cachedb

import (
	"errors"
	"time"

	"github.com/naokichau/nerv/nerv-node/logging"
	cache "github.com/patrickmn/go-cache"
)

type dbcache struct {
	DB *cache.Cache
	// sync.Mutex
}

var (
	UserMsgCache dbcache
	AppUserCache dbcache
)

func (cache *dbcache) Del(key string) {
	cache.DB.Delete(key)
}

func (cache *dbcache) Get(key string) (interface{}, int, error) {
	if value, ttl, exist := cache.DB.GetWithExpiration(key); exist {
		return value, ttl.Minute(), nil
	}
	return nil, 0, errors.New("key not exist")
}

func (cache *dbcache) Set(key string, value interface{}, timeout int, writeType int) error {
	//writeType
	// 0: no overwrite
	// 1: overwrite
	// 2: append
	switch writeType {
	case 0:
		if _, ok := cache.DB.Get(key); ok {
			return errors.New("key already exist")
		}
		var list []interface{}
		list = append(list, value)
		cache.DB.Set(key, list, time.Duration(timeout)*time.Minute)
	case 1:
		var list []interface{}
		list = append(list, value)
		cache.DB.Set(key, list, time.Duration(timeout)*time.Minute)
	case 2:
		var list []interface{}
		if itemList, ok := cache.DB.Get(key); ok {
			itemList = append(itemList.([]interface{}), value)
			list = itemList.([]interface{})
		} else {
			list = append(list, value)
		}
		cache.DB.Set(key, list, time.Duration(timeout)*time.Minute)
	default:
		return errors.New("unknown write type")
	}
	return nil
}

func Create() {
	UserMsgCache.DB = cache.New(5*time.Minute, 10*time.Minute)
	AppUserCache.DB = cache.New(5*time.Minute, 10*time.Minute)

	// UserMsgCache.Set("test", "678uyjhg", 0, 2)
	// list, _, _ := UserMsgCache.Get("test")
	// fmt.Println(list.([]interface{})[2])

	logging.Success("CacheDB created!")
}
