package nervdb

import (
	"bytes"
	"encoding/gob"
	"log"
	"os"
	"sync"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/errors"
	"github.com/syndtr/goleveldb/leveldb/filter"
	"github.com/syndtr/goleveldb/leveldb/opt"
)

type DBcontrol struct {
	dataFile string
	DB       *leveldb.DB
	sync.Mutex
}

func (dbctrl *DBcontrol) Init(dbname string) error {
	var err error
	dbctrl.dataFile = dbname
	file := config.Data.DataDir + "/" + dbname
	dbctrl.DB, err = leveldb.OpenFile(file, &opt.Options{
		OpenFilesCacheCapacity: 16,
		BlockCacheCapacity:     16 / 2 * opt.MiB,
		WriteBuffer:            16 / 4 * opt.MiB, // Two of these are used internally
		Filter:                 filter.NewBloomFilter(10),
	})
	if _, corrupted := err.(*errors.ErrCorrupted); corrupted {
		dbctrl.DB, err = leveldb.RecoverFile(file, nil)
	}
	// defer dbctrl.db.Close()
	if err != nil {
		return err
	}
	return nil
}

func (dbctrl *DBcontrol) Put(key []byte, value interface{}) error {
	var data bytes.Buffer
	enc := gob.NewEncoder(&data)
	err := enc.Encode(value)
	if err != nil {
		log.Fatal("encode error:", err)
	}
	return dbctrl.DB.Put(key, data.Bytes(), nil)
}

func (dbctrl *DBcontrol) Has(key []byte) (bool, error) {
	return dbctrl.DB.Has(key, nil)
}

func (dbctrl *DBcontrol) Get(key []byte) ([]byte, error) {
	dat, err := dbctrl.DB.Get(key, nil)
	if err != nil {
		return nil, err
	}
	return dat, nil
}

func (dbctrl *DBcontrol) Delete(key []byte) error {
	return dbctrl.DB.Delete(key, nil)
}

func (dbctrl *DBcontrol) Clean() error {
	dbctrl.DB.Close()
	err := os.RemoveAll(config.Data.DataDir + "/" + dbctrl.dataFile)
	if err != nil {
		return err
	}
	err = dbctrl.Init(dbctrl.dataFile)
	if err != nil {
		return err
	}
	return nil
}
