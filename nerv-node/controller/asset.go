package controller

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"os"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/constvar"
	"github.com/naokichau/nerv/nerv-node/logging"
)

func (ctrl *Controller) updateAssetDemand() {
	ctrl.AssetDemandTable.Lock()
	defer ctrl.AssetDemandTable.Unlock()
	iter := ctrl.AssetDemandTable.DB.NewIterator(nil, nil)
	var ctx AssetDemandCtx
	for iter.Next() {
		key := iter.Key()
		value := iter.Value()
		dec := gob.NewDecoder(bytes.NewBuffer(value))
		err := dec.Decode(&ctx)
		if err != nil {
			logging.Error(constvar.ErrUnexpected, "Can't read data in database. Detail:", err)
		}
		// fmt.Println(string(key), ctx)
		if ctx.Times > 80 {
			if ctx.AvalStatus == 0 {
				if len(ctrl.CurrentlyRetrieve.Files) == config.Data.Policy.Retriever.MaxActiveRetriever {
					continue
				}
				ctx.AvalStatus = 1
				go ctrl.retrieveFile(string(key), ctx.Size, ctx.Holders)
			}
		} else {
			if ctx.Owner != config.Data.Owner { // only check file that are not belong to the node owner
				if ctx.Times < 20 {
					if ctx.AvalStatus != 1 {
						if ctx.AvalStatus == 2 {
							// delete the file
							err = os.Remove(config.Data.DataDir + "/storage/" + ctx.Owner + "/" + ctx.LocalLink)
							if err != nil {
								logging.Error(constvar.ErrUnexpected, "Cant delete file. Detail:", err)
							}
						}
						ctrl.AssetDemandTable.Delete(key)
						continue
					}
				} else {
					if ctx.AvalStatus == 2 {
						// delete the file
						err = os.Remove(config.Data.DataDir + "/storage/" + ctx.Owner + "/" + ctx.LocalLink)
						if err != nil {
							logging.Error(constvar.ErrUnexpected, "Can't delete file. Detail:", err)
						}
						ctx.AvalStatus = 0
					}
					// still keep where this file can be get if user try to retrieve the file from this node
				}
			}
		}
		ctx.Times = 0
		err = ctrl.AssetDemandTable.Put(key, ctx)
		if err != nil {
			logging.Error(constvar.ErrUnexpected, "Error update asset demand. Detail:", err)
		}
	}
	iter.Release()
	err := iter.Error()
	if err != nil {
		fmt.Println(err)
	}
	err = ctrl.AssetDemandTable.Clean()
	if err != nil {
		fmt.Println(err)
	}
}

func (ctrl *Controller) GetAssetLink(hash string) (string, error) {
	assetCtxByte, err := ctrl.AssetDemandTable.Get([]byte(hash))
	if err != nil {
		return "", errors.New("asset not found")
	}
	var assetCtx AssetDemandCtx
	dec := gob.NewDecoder(bytes.NewBuffer(assetCtxByte))
	err = dec.Decode(&assetCtx)
	if assetCtx.AvalStatus == 2 {
		return "/" + assetCtx.Owner + assetCtx.LocalLink, nil
	}
	return "", errors.New("asset not found")
}
