package controller

import (
	"sync"
	"time"

	"github.com/naokichau/nerv/nerv-node/database/nervdb"
	"github.com/naokichau/nerv/nerv-node/types"
)

type Controller struct {
	Params            Params
	PeerTable         nervdb.DBcontrol
	AppInfoTable      nervdb.DBcontrol
	AppDemandTable    nervdb.DBcontrol
	AssetDemandTable  nervdb.DBcontrol
	CurrentlyRetrieve struct {
		Files map[string]*retriever
		sync.Mutex
	}
}

type Params struct {
	DataDir string
	NodeID  string
}

type AssetDemandCtx struct {
	Holders    map[string]types.NodeNetworkInfo
	LocalLink  string
	AvalStatus byte
	Size       int64
	Owner      string
	Times      int
	CreatedAt  time.Time
}
type FunctionDemandCtx struct {
	Ftype int
	AppID string
	Times int
}

type retrievingConn struct {
	DataSource   string
	DataSourceID string
	Part         int
}

type retriever struct {
	RetrieveCs   []*retrievingConn
	FileLink     string
	FileSize     int64
	FilePartSize int
	DataSources  map[string]types.NodeNetworkInfo
	PartDone     chan struct {
		IsSuccess bool
		Data      []byte
		Part      int
	}
	NodeID       string
	DownloadDone chan struct{}
}
