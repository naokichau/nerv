package controller

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/nervnet/netutils"
	"github.com/naokichau/nerv/nerv-node/types"
)

func (ctrl *Controller) retrieveFile(fileLink string, fileSize int64, dataSources map[string]types.NodeNetworkInfo) {
	ctrl.CurrentlyRetrieve.Lock()
	var r *retriever
	var ok bool
	if r, ok = ctrl.CurrentlyRetrieve.Files[fileLink]; ok {
		ctrl.CurrentlyRetrieve.Unlock()
		return
	}
	r.RetrieveCs = make([]*retrievingConn, config.Data.Policy.Retriever.MaxConnPerFile)
	ctrl.CurrentlyRetrieve.Files[fileLink] = r
	ctrl.CurrentlyRetrieve.Unlock()
	r.FilePartSize = int(fileSize) / config.Data.Policy.Retriever.MaxConnPerFile
	r.startRetriever()

	var part int
	for {
		for dataSourceID, dataSource := range dataSources {
			nodeLatency := netutils.IsAcceptablePing(dataSource.IPs, strconv.Itoa(dataSource.NodePort), ctrl.Params.NodeID)
			if nodeLatency.IP == "" {
				continue
			}
			if nodeLatency.NodeID != dataSourceID {
				// ?????
			}
			okIP := nodeLatency.IP
			rC := &retrievingConn{
				DataSource:   okIP + ":" + strconv.Itoa(dataSource.NodePort),
				DataSourceID: dataSourceID,
				Part:         part,
			}
			r.DataSources[rC.DataSourceID] = dataSource
			go r.retrieve(rC)
			part++
			if part == config.Data.Policy.Retriever.MaxConnPerFile {
				break
			}
		}
		if part == config.Data.Policy.Retriever.MaxConnPerFile {
			break
		}
	}
	return
}

func (fileRetriever *retriever) retrieve(rC *retrievingConn) error {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", "http://"+rC.DataSource+"/assets/"+fileRetriever.FileLink, nil)
	rangeHeader := "bytes=" + strconv.Itoa(rC.Part*fileRetriever.FilePartSize) + "-" + strconv.Itoa((rC.Part+1)*fileRetriever.FilePartSize)
	req.Header.Add("Range", rangeHeader)
	var result struct {
		IsSuccess bool
		Data      []byte
		Part      int
	}
	fileRetriever.RetrieveCs[rC.Part] = rC
	resp, err := client.Do(req)
	if err != nil {
		fileRetriever.RetrieveCs[rC.Part] = nil
		result.IsSuccess = false
		result.Part = rC.Part
		fileRetriever.PartDone <- result
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		result.IsSuccess = false
		result.Part = rC.Part
		fileRetriever.RetrieveCs[rC.Part] = nil
		fileRetriever.PartDone <- result
		return err
	}
	reader, _ := ioutil.ReadAll(resp.Body)
	result.IsSuccess = true
	result.Data = reader
	result.Part = rC.Part
	fileRetriever.RetrieveCs[rC.Part] = nil
	fileRetriever.PartDone <- result
	return nil
}

func (fileRetriever *retriever) startRetriever() {
	fileRetriever.PartDone = make(chan struct {
		IsSuccess bool
		Data      []byte
		Part      int
	})
	fileRetriever.DownloadDone = make(chan struct{})
	assetParts := make([][]byte, config.Data.Policy.Retriever.MaxConnPerFile)
	go func() {
		var donePart int
		for {
			select {
			case partReceived := <-fileRetriever.PartDone:
				if !partReceived.IsSuccess {
					//Retrieve failed
					if fileRetriever.RetrieveCs[partReceived.Part] != nil {
						continue
					}
					go func(part int) {
						for dataSourceID, dataSource := range fileRetriever.DataSources {
							nodeLatency := netutils.IsAcceptablePing(dataSource.IPs, strconv.Itoa(dataSource.NodePort), fileRetriever.NodeID)
							if nodeLatency.IP == "" {
								continue
							}
							if nodeLatency.NodeID != dataSourceID {
								// ?????
							}
							okIP := nodeLatency.IP
							rC := &retrievingConn{
								DataSource:   okIP + ":" + strconv.Itoa(dataSource.NodePort),
								DataSourceID: dataSourceID,
								Part:         part,
							}
							err := fileRetriever.retrieve(rC)
							if err == nil {
								return
							}
						}
					}(partReceived.Part)
				} else {
					//Retrieve success
					// ??? should write filepart to disk
					donePart++
					assetParts[partReceived.Part] = partReceived.Data
					if donePart == config.Data.Policy.Retriever.MaxConnPerFile {
						close(fileRetriever.DownloadDone)
						return
					}
				}
			}
		}
	}()
	<-fileRetriever.DownloadDone
	var finalAsset []byte
	for _, assetPart := range assetParts {
		finalAsset = append(finalAsset, assetPart...)
	}
	fmt.Println(string(finalAsset))
}
