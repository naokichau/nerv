package controller

import (
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/database/storage"
	"github.com/naokichau/nerv/nerv-node/logging"
	"github.com/naokichau/nerv/nerv-node/types"
	"github.com/naokichau/nerv/nerv-node/utils"
	yaml "gopkg.in/yaml.v2"
)

// TODO:
// + Create a map file for app
//

func (ctrl *Controller) updateAppDemand() {
	ctrl.AppDemandTable.Lock()
	defer ctrl.AppDemandTable.Unlock()
}

func (ctrl *Controller) DeployUpdate(dataForm *multipart.Form) (string, error) {
	return "", nil
}

func (ctrl *Controller) Deploy(r *http.Request) (string, error) {
	appPubKey := r.FormValue("appID")
	signature := r.FormValue("signature")
	_, appPkg, _ := r.FormFile("appPkg")
	_, err := ctrl.loadAppPkg(appPkg, appPubKey, signature)
	if err != nil {
		return "", errors.New("An error has occurred with config package. Detail: " + err.Error())
	}
	return "All check OK!", nil
}

func (ctrl *Controller) loadAppPkg(pkg *multipart.FileHeader, appPubKey string, signature string) (*types.AppPkg, error) {
	newAppPkg := &types.AppPkg{
		FunctionFiles: make(map[string]*zip.File),
		AssetFiles:    make(map[string]*zip.File),
	}
	pkgZip, err := pkg.Open()
	if err != nil {
		return nil, errors.New("Can't read package")
	}
	pkgBuf := bytes.NewBuffer(nil)
	if _, err := io.Copy(pkgBuf, pkgZip); err != nil {
		return nil, err
	}
	// pubKeyBytes, _ := base32.StdEncoding.DecodeString(appPubKey)
	// sigBytes, _ := base64.StdEncoding.DecodeString(signature)
	pkgCtn, err := zip.NewReader(pkgZip, pkg.Size)
	if err != nil {
		return nil, errors.New("Can't read package")
	}

	defer pkgZip.Close()

	// Look for app.yml first
	for _, f := range pkgCtn.File {
		if !f.FileInfo().IsDir() && f.FileInfo().Name() == "app.yml" {
			// var rc io.ReadCloser
			fpath := config.Data.DataDir + "/storage/1" + appPubKey + "/"
			rc, err := f.Open()
			if err != nil {
				return nil, err
			}
			fileData, err := storage.ReadFileZip(rc)
			if err != nil {
				rc.Close()
				return nil, errors.New("Can't read app.yml")
			}
			// appYmlHash := crypto.HashSHA256(fileData)
			// if !crypto.VerifySignature(pubKeyBytes, appYmlHash[:], sigBytes) {
			// 	rc.Close()
			// 	return nil, errors.New("Invalid app.yml signature")
			// }
			err = yaml.Unmarshal(fileData, &newAppPkg.Info)
			if err != nil {
				fmt.Println(err)
				rc.Close()
				return nil, errors.New("Can't read content of app.yml")
			}
			if newAppPkg.Info.Address != appPubKey {
				rc.Close()
				return nil, errors.New("App's address in app.yml is difference from app.yml address!")
			}
			rc.Close()
			err = utils.WriteFiletoDisk(f.FileInfo().Name(), fpath, fileData, f.Mode())
			if err != nil {
				os.RemoveAll(config.Data.DataDir + "/storage/1" + appPubKey)
				return &types.AppPkg{}, err
			}
		}
	}

	for _, f := range pkgCtn.File {
		if !f.FileInfo().IsDir() {
			fileInfo := strings.Split(f.Name, "/")
			// fileName := strings.Split(f.FileInfo().Name(), ".")[0]
			// fhash := crypto.HashSHA256base32([]byte(appPubKey + fileName))
			fpath := config.Data.DataDir + "/storage/1" + appPubKey + "/"
			var rc io.ReadCloser
			var fileData []byte
			// Identify file and run validation
			switch fileInfo[1] {
			case "appfiles":
				switch fileInfo[2] {
				case "functions":
					if fileInfo[3] == "js" || fileInfo[3] == "binary" || fileInfo[3] == "container" {
						rc, err = f.Open()
						if err != nil {
							return nil, errors.New("Can't open file " + f.FileInfo().Name())
						}
						fileData, _ = storage.ReadFileZip(rc)
					} else {
						rc.Close()
						logging.Warn("Detect file the shouldn't be in this directory: " + f.Name)
						continue
					}
					switch fileInfo[3] {
					case "js":
						fpath = fpath + "function/js/"
					case "binary":
						fpath = fpath + "function/binary/"
					case "container":
						fpath = fpath + "function/container/"
					}
				case "procedures.yml":
					rc, err = f.Open()
					if err != nil {
						return nil, errors.New("Can't open file " + f.FileInfo().Name())
					}
					fileData, _ = storage.ReadFileZip(rc)
					err = yaml.Unmarshal(fileData, &newAppPkg.Procedures)
					if err != nil {
						fmt.Println(err)
						rc.Close()
						continue
					}
				case "services.yml":
					rc, err = f.Open()
					if err != nil {
						return nil, errors.New("Can't open file " + f.FileInfo().Name())
					}
					fileData, _ = storage.ReadFileZip(rc)
				case "assets":
					rc, err = f.Open()
					if err != nil {
						return nil, errors.New("Can't open file " + f.FileInfo().Name())
					}
					fileData, _ = storage.ReadFileZip(rc)
					fpath = fpath + "assets/"
				case "web.zip":
					rc, err = f.Open()
					if err != nil {
						return nil, errors.New("Can't open file " + f.FileInfo().Name())
					}
					fileData, _ = storage.ReadFileZip(rc)
				default:
					logging.Warn("Detect file the shouldn't be in this directory: " + f.Name)
					continue
				}
			default:
				continue
			}
			rc.Close()
			err = utils.WriteFiletoDisk(f.FileInfo().Name(), fpath, fileData, f.Mode())
			if err != nil {
				os.RemoveAll(config.Data.DataDir + "/storage/1" + appPubKey)
				return &types.AppPkg{}, err
			}
		}
	}
	appInfo := types.App{
		Address:     newAppPkg.Info.Address,
		Name:        newAppPkg.Info.Name,
		Description: newAppPkg.Info.Description,
		Version:     newAppPkg.Info.Version,
		Checksums:   newAppPkg.Info.Checksums,
		Signature:   signature,
		UpdatedAt:   time.Now(),
	}
	ctrl.AppInfoTable.Put([]byte("1"+appPubKey), appInfo)
	return newAppPkg, nil
}
