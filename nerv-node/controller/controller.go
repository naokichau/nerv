package controller

import (
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"encoding/hex"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/naokichau/nerv/nerv-node/crypto"
	"github.com/naokichau/nerv/nerv-node/database/storage"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/constvar"
	"github.com/naokichau/nerv/nerv-node/logging"
	"github.com/naokichau/nerv/nerv-node/types"
)

func must(e error) {
	if e != nil {
		logging.Fatal(constvar.ErrDataSync, "Failed to init db", e)
	}
}

func (ctrl *Controller) Init() {
	logging.Log("Starting controllers...")
	must(ctrl.PeerTable.Init("peer"))
	must(ctrl.AppInfoTable.Init("appInfo"))
	must(ctrl.AppDemandTable.Init("appDemand"))
	must(ctrl.AssetDemandTable.Init("assetDemand"))
	ctrl.CurrentlyRetrieve.Files = make(map[string]*retriever, config.Data.Policy.Retriever.MaxActiveRetriever)
	err := ctrl.scanFileStorage()
	if err != nil {
		logging.Fatal(constvar.ErrUnexpected, "Encounter error while scanning datadir", err)
		return
	}
	go func() {
		var assetUpdateInterval time.Duration = 5
		var appUpdateInterval time.Duration = 30
		assetTicker := time.NewTicker(assetUpdateInterval * time.Second)
		appTicker := time.NewTicker(appUpdateInterval * time.Minute)
		go func() {
			for {
				select {
				case <-assetTicker.C:
					logging.Log("Refreshing assets demand...")
					start := time.Now()
					ctrl.updateAssetDemand()
					elapsed := time.Since(start)
					logging.Log("Refreshed assets demand. Elapsed time: " + fmt.Sprint(elapsed))
				}
			}
		}()
		for {
			select {
			case <-appTicker.C:
				logging.Log("Refreshing apps demand...")
				start := time.Now()
				ctrl.updateAppDemand()
				elapsed := time.Since(start)
				logging.Log("Refreshed apps demand. Elapsed time: " + fmt.Sprint(elapsed))
			}
		}
	}()
}

func (ctrl *Controller) IncreaseDemand(demandCtx interface{}, demandType int) {
	// assetType:
	// 0: asset
	// 1: app (web/function/procedure/service)
	if demandType == 0 {
		var ctx AssetDemandCtx
		ctx.Holders = make(map[string]types.NodeNetworkInfo)
		type asset struct {
			AssetLink types.AssetLink
			Owner     string
		}
		ctrl.AssetDemandTable.Lock()
		defer ctrl.AssetDemandTable.Unlock()
		asctx := demandCtx.(*asset)
		astLink := []byte(asctx.AssetLink.Link)
		data, err := ctrl.AssetDemandTable.Get(astLink)
		if err != nil {
			ctx.Owner = asctx.Owner
			ctx.Times = 1
			ctx.Size = asctx.AssetLink.Size
			ctx.AvalStatus = 0
			ctx.Holders[asctx.AssetLink.NodeID] = asctx.AssetLink.NodeNetInfo
			ctx.CreatedAt = time.Now()
			err = ctrl.AssetDemandTable.Put(astLink, ctx)
			if err != nil {
				logging.Error(constvar.ErrUnexpected, "Error update asset demand. Detail:", err)
			}
			return
		}
		dec := gob.NewDecoder(bytes.NewBuffer(data))
		err = dec.Decode(&ctx)
		if err != nil {
			logging.Error(constvar.ErrUnexpected, "Can't read data in database. Detail:", err)
		}
		// skip checking stuff for now
		ctx.Times++
		ctx.Holders[asctx.AssetLink.NodeID] = asctx.AssetLink.NodeNetInfo
		err = ctrl.AssetDemandTable.Put(astLink, ctx)
		if err != nil {
			logging.Error(constvar.ErrUnexpected, "Error update asset demand. Detail:", err)
		}
	} else {
		var ctx FunctionDemandCtx
		type functionCtx struct {
			FHash string
			Ftype int
			AppID string
		}
		ctrl.AppDemandTable.Lock()
		defer ctrl.AppDemandTable.Unlock()
		fnctx := demandCtx.(*functionCtx)
		fnHash := []byte(fnctx.FHash)
		data, err := ctrl.AppDemandTable.Get(fnHash)
		if err != nil {
			ctx.Ftype = fnctx.Ftype
			ctx.AppID = fnctx.AppID
			ctx.Times = 1
			err = ctrl.AppDemandTable.Put(fnHash, ctx)
			if err != nil {
				logging.Error(constvar.ErrUnexpected, "Error update app demand. Detail:", err)
			}
			return
		}
		dec := gob.NewDecoder(bytes.NewBuffer(data))
		err = dec.Decode(&ctx)
		if err != nil {
			logging.Error(constvar.ErrUnexpected, "Can't read data in database", err)
		}
		ctx.Times++
		err = ctrl.AppDemandTable.Put(fnHash, ctx)
		if err != nil {
			logging.Error(constvar.ErrUnexpected, "Error update app demand", err)
		}
	}
}

func (ctrl *Controller) scanFileStorage() error {
	var tempAppInfo map[string]types.App
	tempAppInfo = make(map[string]types.App)
	files, err := storage.ScanFolder(config.Data.DataDir + "/storage/")
	if err != nil {
		return err
	}
	sort.Strings(files)
	for _, file := range files {
		fileInfo := strings.Split(strings.Split(file, config.Data.DataDir+"/storage/")[1], "/")
		assetCtx := AssetDemandCtx{
			LocalLink:  strings.Join(fileInfo[1:], "/"),
			Owner:      fileInfo[0],
			Times:      0,
			AvalStatus: 2,
			Holders:    make(map[string]types.NodeNetworkInfo),
		}

		switch string(fileInfo[0][0]) {
		case "0":
			// ??? User address

		case "1":
			// App address
			if fileInfo[1] == "app.yml" {
				var appInfo types.App
				appInfoByte, err := ctrl.AppInfoTable.Get([]byte(fileInfo[0]))
				if err != nil {
					fmt.Println(err)
					// ??? Remove file
					continue
				}
				dec := gob.NewDecoder(bytes.NewBuffer(appInfoByte))
				err = dec.Decode(&appInfo)
				if err != nil {
					logging.Error(constvar.ErrUnexpected, "Can't read data in database", err)
				}
				tempAppInfo[fileInfo[0]] = appInfo

				fileData, err := storage.ReadfileFromSource(file)
				if err != nil {
					return err
				}

				assetCtx.Size = int64(len(fileData))
				assetCtx.Holders[config.HostID] = config.Network

				fileHash := crypto.HashSHA256([]byte(fileInfo[0] + "app.yml"))
				ctrl.AssetDemandTable.Put([]byte(base64.StdEncoding.EncodeToString(fileHash[:])), assetCtx)
			} else {
				if _, ok := tempAppInfo[fileInfo[0]]; !ok {
					// ??? Remove files
					// app.yml must exist in AppInfoTable before other file exist
					continue
				}
				fileData, err := storage.ReadfileFromSource(file)
				if err != nil {
					return err
				}
				fileLink := strings.Join(fileInfo[1:], "/")
				fileDataHash := crypto.HashSHA256(fileData)
				if hex.EncodeToString(fileDataHash[:]) != tempAppInfo[fileInfo[0]].Checksums[fileLink] {
					// ??? Remove file
				}

				assetCtx.Size = int64(len(fileData))
				assetCtx.Holders[config.HostID] = config.Network

				fileHash := crypto.HashSHA256([]byte(fileInfo[0] + fileLink))
				ctrl.AssetDemandTable.Put([]byte(base64.StdEncoding.EncodeToString(fileHash[:])), assetCtx)
			}
		}
	}
	return nil
}
