package validator

// import (
// 	"errors"
// 	"fmt"
// 	"net/url"
// 	"regexp"
// 	"strings"

// 	"github.com/naokichau/nerv/nerv-node/crypto"
// 	"github.com/naokichau/nerv/nerv-node/types"
// )

// func ValidateDevicePkg(data *types.NamespacePkg) error {
// 	dmHash, _ := crypto.HashSHA(data.OriginalCfgFile["devicemodels.yml"])
// 	if data.Info.DeviceModelsHash != fmt.Sprintf("%x", dmHash) {
// 		return errors.New("file devicemodels.yml has different hash from nerv.yml")
// 	}
// 	for deviceModel, deviceModelInfo := range data.DeviceModels {
// 		//check firmware hash
// 		if firmware, ok := data.FirmwaresFile[deviceModel]; ok {
// 			fileByte, err := file.ReadFileZip(firmware)
// 			if err != nil {
// 				return errors.New("can't read file " + deviceModel)
// 			}
// 			fileHash, _ := crypto.HashSHA(fileByte)
// 			if deviceModelInfo.FirmwareHash != fmt.Sprintf("%x", fileHash) {
// 				return errors.New("firmware file " + deviceModel + " has different hash from devicemodels.yml")
// 			}
// 		} else {

// 		}

// 		if deviceModelInfo.StatusUpdateInterval < 0 {
// 			return errors.New("device model " + deviceModel + " has invalid status update interval!")
// 		}
// 		if deviceModelInfo.Type != "node-arm" && deviceModelInfo.Type != "hub" && deviceModelInfo.Type != "node-mcu" {
// 			return errors.New("device model " + deviceModel + " has unknown device Type!")
// 		}

// 		if strings.Contains(strings.Join(deviceModelInfo.EnrollPolicy, ","), "LOCAL_DEVICE_LIST") && strings.Contains(strings.Join(deviceModelInfo.EnrollPolicy, ","), "USE_VALIDATOR") {
// 			return errors.New("device model " + deviceModel + " has invalid enroll policy!")
// 		}
// 		for _, policy := range deviceModelInfo.EnrollPolicy {
// 			policyDetail := strings.Split(policy, ":")
// 			switch policyDetail[0] {
// 			case "LOCAL_DEVICE_LIST":
// 				if _, ok := data.DeviceLists[deviceModel]; !ok {
// 					return errors.New("device list file " + deviceModel + "doesn't exist")
// 				}
// 				fileHash, _ := crypto.HashSHA(data.DeviceListsFile[deviceModel])
// 				if policyDetail[1] != fmt.Sprintf("%x", fileHash) {
// 					return errors.New("device list file " + deviceModel + " has different hash from devicemodels.yml")
// 				}
// 			case "USE_VALIDATOR":
// 				if len(deviceModelInfo.ValidatorEndPoints) == 0 {
// 					return errors.New("Device model " + deviceModel + " doesn't have any validator endpoints!")
// 				}
// 				for _, endpoint := range deviceModelInfo.ValidatorEndPoints {
// 					_, err := url.ParseRequestURI(endpoint)
// 					if err != nil {
// 						return errors.New("Device model " + deviceModel + " has invalid endpoint!")
// 					}
// 				}
// 			default:
// 				if policy != "UPDATE_FW_ONCONNECT" {
// 					return errors.New("Device model " + deviceModel + " has unknown enroll policy!")
// 				}
// 			}
// 		}
// 		// } else {
// 		// 	return errors.New("Device model " + deviceModel + " doesn't exist!")
// 		// }
// 	}
// 	return nil
// }

// func ValidateFunctionPkg(data *types.NamespacePkg) error {
// 	fnHash, _ := crypto.HashSHA(data.OriginalCfgFile["functions.yml"])
// 	if data.Info.FunctionsHash != fmt.Sprintf("%x", fnHash) {
// 		return errors.New("file functions.yml has different hash from nerv.yml")
// 	}
// 	for function, functionInfo := range data.Functions {
// 		if functionInfo.Type != "binary" && functionInfo.Type != "container" {
// 			return errors.New("Function " + function + " has invalid type")
// 		}
// 		for _, arch := range functionInfo.Hash {
// 			archInfo := strings.Split(arch, ":")
// 			fileByte, err := file.ReadFileZip(data.FunctionsFile[function+"_"+archInfo[0]])
// 			if err != nil {
// 				return errors.New("can't read file " + function + "_" + archInfo[0])
// 			}
// 			fnHash, _ := crypto.HashSHA(fileByte)
// 			if archInfo[1] != fmt.Sprintf("%x", fnHash) {
// 				return errors.New("file " + function + "_" + archInfo[0] + " has different hash from functions.yml")
// 			}
// 		}
// 	}
// 	return nil
// }

// func ValidatePipelinePkg(data *types.NamespacePkg) error {
// 	plHash, _ := crypto.HashSHA(data.OriginalCfgFile["pipelines.yml"])
// 	if data.Info.PipelinesHash != fmt.Sprintf("%x", plHash) {
// 		return errors.New("file pipelines.yml has different hash from nerv.yml")
// 	}
// 	for pipeline, pipelineInfo := range data.Pipelines {
// 		if len(pipelineInfo.ProcessTags) == 0 {
// 			return errors.New("Pipeline '" + pipeline + "' doesn't have any process_tag!")
// 		}
// 		if len(pipelineInfo.SupportArchs) == 0 {
// 			for _, arch := range pipelineInfo.SupportArchs {
// 				if arch != "amd64" && arch != "i386" && arch != "arm" && arch != "arm64" {
// 					return errors.New("Pipeline '" + pipeline + "' has invalid architecture '" + arch + "' !")
// 				}
// 			}
// 			return errors.New("Pipeline '" + pipeline + "' doesn't have any architecture!")
// 		}
// 		if len(pipelineInfo.Stages) == 0 {
// 			return errors.New("Pipeline '" + pipeline + "' doesn't have any stage!")
// 		}
// 		prevPipeNum := 0
// 		for _, stage := range pipelineInfo.Stages {
// 			stagePipes := strings.Split(stage, "|")
// 			if len(stagePipes) < prevPipeNum {
// 				fmt.Println(stagePipes)
// 				fmt.Println(len(stagePipes), prevPipeNum)
// 				return errors.New("Pipeline '" + pipeline + "' has invalid format!")
// 			}
// 			prevPipeNum = 0
// 			for _, pipeGroup := range stagePipes {
// 				pipes := strings.Split(pipeGroup, ":")
// 				for _, pipe := range pipes {
// 					if strings.LastIndex(pipe, "SENDTO") == 0 {
// 						var re = regexp.MustCompile(`SENDTO\((.*)\)`)
// 						parsedContext := re.FindStringSubmatch(pipe)
// 						if len(parsedContext) == 0 {
// 							return errors.New("Function '" + pipe + "' of pipeline '" + pipeline + "' is invalid!")
// 						}
// 						queryParams := strings.Split(parsedContext[1], ";")
// 						for _, queryParam := range queryParams {
// 							fmt.Println(queryParam)
// 						}
// 					} else {
// 						if pipe != "SENDBACK" {
// 							if hasSymbolOrSpace(pipe) {
// 								return errors.New("Function '" + pipe + "' of pipeline '" + pipeline + "' is invalid!")
// 							}
// 							if len(pipe) == 0 {
// 								continue
// 							}
// 							if _, ok := data.Functions[pipe]; ok {
// 								for _, arch := range pipelineInfo.SupportArchs {
// 									if fncFile, ok := data.FunctionsFile[pipe+"_"+arch]; ok {
// 										if _, ok := data.FunctionsNeedFile[pipe+"_"+arch]; !ok {
// 											data.FunctionsNeedFile[pipe] = fncFile
// 										}
// 									} else {
// 										return errors.New("Function '" + pipe + "' doesn't support '" + arch + "' architecture!")
// 									}
// 								}
// 							} else {
// 								return errors.New("Function '" + pipe + "' of pipeline '" + pipeline + "' is invalid!")
// 							}
// 						}
// 					}
// 					prevPipeNum++
// 				}
// 			}
// 		}
// 	}
// 	return nil
// }
