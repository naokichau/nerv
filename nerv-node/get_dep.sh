#!/bin/sh
apk --update add git
cd /go/src/github.com/naokichau/nerv/nerv-node/
(
    set -e
    go get -v
)
errorCode=$?
if [ $errorCode -ne 0 ]; then
    echo "Error getting dependencies!"
    tail -f /dev/null
    exit $errorCode
fi
echo "Get all dependencies successful!"
tail -f /dev/null