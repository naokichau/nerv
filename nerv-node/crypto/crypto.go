package crypto

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"io"
)

func EncryptDecrypt(input []byte, key []byte, encrypt bool) ([]byte, error) {
	return input, nil
}

func encrypt(key []byte, data string) ([]byte, error) {
	plaindata := []byte(data)

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaindata))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return nil, err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaindata)

	return ciphertext, nil
}

func decrypt(key []byte, cryptoData []byte) ([]byte, error) {

	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	if len(cryptoData) < aes.BlockSize {
		return nil, errors.New("cipherdata too short")
	}
	iv := cryptoData[:aes.BlockSize]
	cryptoData = cryptoData[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	stream.XORKeyStream(cryptoData, cryptoData)

	return cryptoData, nil
}
