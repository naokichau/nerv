package crypto

import (
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base32"
)

func HashSHA(data []byte) [32]byte {
	hashByte := sha512.Sum512_256(data)
	return hashByte
}

func HashSHA256(data []byte) [32]byte {
	hashByte := sha256.Sum256(data)
	return hashByte
}

func HashSHA256base32(data []byte) string {
	hashByte := sha256.Sum256(data)
	return base32.StdEncoding.EncodeToString(hashByte[:])
}
