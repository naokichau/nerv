package crypto

import (
	"bytes"
	"encoding/base64"

	"golang.org/x/crypto/ed25519"
)

type KeyPair struct {
	PrivateKey []byte
	PublicKey  []byte
}

func (key *KeyPair) GenerateKey(seed []byte) (*KeyPair, error) {
	var err error
	key.PublicKey, key.PrivateKey, err = ed25519.GenerateKey(bytes.NewBuffer(seed))
	if err != nil {
		return key, err
	}
	return key, nil
}

func (key *KeyPair) Import(privateKey string) (*KeyPair, error) {
	newKey := ed25519.PrivateKey{}
	newKey, err := base64.StdEncoding.DecodeString(privateKey)
	if err != nil {
		return key, err
	}
	key.PublicKey = newKey.Public().(ed25519.PublicKey)
	key.PrivateKey = newKey
	return key, nil
}

func (key *KeyPair) Verify(data, signature []byte) (bool, error) {
	isValid := ed25519.Verify(key.PublicKey, data, signature)
	return isValid, nil
}

func (key *KeyPair) Sign(data []byte) ([]byte, error) {
	result := ed25519.Sign(key.PrivateKey, data)
	return result, nil
}
