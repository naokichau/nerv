package logging

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/fatih/color"
)

// Error code list
//
//
//
var DataDir string

func saveLog(log string) error {
	WHERETOSAVE := "local"
	switch WHERETOSAVE {
	case "local":
		f, err := os.OpenFile(DataDir+"/serverLog.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			println("error opening file: %v", err)
			return err
		}
		defer f.Close()
		w := bufio.NewWriter(f)
		fmt.Fprintln(w, log)
		return w.Flush()
	case "monitor":
		return nil
	default:
		fmt.Println("quit")
		return nil
	}
}

// Error func: print error msg and log it
func Error(code string, msg string, errDetail error) {
	c := color.New(color.FgHiRed, color.Bold)
	erCode := "ER:" + code
	errStr := "   [T:" + fmt.Sprintf("%d", time.Now().Unix()) + "]" + " [M:" + msg + "] [D:" + errDetail.Error() + "]"
	fmt.Println(c.Sprint(erCode) + errStr)
	saveLog(erCode + errStr)
}

// Info func: print info msg and log it
func Log(msg string) {
	c := color.New(color.FgHiYellow, color.Bold)
	logStr := "      [T:" + fmt.Sprintf("%d", time.Now().Unix()) + "]" + " [M:" + msg + "]"
	fmt.Println(c.Sprint("LOG") + logStr)
	saveLog("LOG" + logStr)
}

// Success func: print info msg and log it
func Success(msg string) {
	c := color.New(color.FgHiCyan, color.Bold)
	successStr := "  [T:" + fmt.Sprintf("%d", time.Now().Unix()) + "]" + " [M:" + msg + "]"
	fmt.Println(c.Sprint("SUCCESS") + successStr)
	saveLog("SUCCESS" + successStr)
}

func Warn(msg string) {
	c := color.New(color.FgHiRed, color.Bold)
	warnStr := "     [T:" + fmt.Sprintf("%d", time.Now().Unix()) + "]" + " [M:" + msg + "]"
	fmt.Println(c.Sprint("WARN") + warnStr)
	saveLog("WARN" + warnStr)
}

// Fail func: print info msg and log it
func Fatal(code string, msg string, errDetail error) {
	c := color.New(color.FgHiRed, color.Bold)
	erCode := "FTL:" + code
	fatalStr := "  [T:" + fmt.Sprintf("%d", time.Now().Unix()) + "]" + " [M:" + msg + "] [D:" + errDetail.Error() + "]"
	fmt.Println(c.Sprint(erCode) + fatalStr)
	saveLog(erCode + fatalStr)
	os.Exit(1)
}

// Info func: print info msg and log it
func NetLog(msg string) {
	c := color.New(color.FgGreen, color.Bold)
	msg = strings.TrimSuffix(msg, "\n")
	logStr := "  [T:" + fmt.Sprintf("%d", time.Now().Unix()) + "]" + " [M:" + msg + "]"
	fmt.Println(c.Sprint("NETWORK") + logStr)
	saveLog("NETWORK" + logStr)
}
