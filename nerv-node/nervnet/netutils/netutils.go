package netutils

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"math"
	"net"
	"net/http"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/naokichau/nerv/nerv-node/types"
)

func GetAllInterfaceIPs() []net.IP {
	var localAddrs []net.IP
	addrs, _ := net.InterfaceAddrs()
	for _, addr := range addrs {
		strAddr := strings.Split(addr.String(), "/")[0]
		localAddrs = append(localAddrs, net.ParseIP(strAddr))
	}
	return localAddrs
}

func IsAcceptablePing(nodeIPs []net.IP, nodePort string, thisNodeID string) types.NodeLatency {
	var latencyList struct {
		List []types.NodeLatency
		sync.Mutex
	}
	var wg sync.WaitGroup
	wg.Add(len(nodeIPs))
	for _, nodeIP := range nodeIPs {
		go func(ip string) {
			nodeLatency, err := Ping(ip, nodePort, thisNodeID)
			if err == nil && nodeLatency.AvgLatency+nodeLatency.AvgDev < 500 {
				latencyList.Lock()
				latencyList.List = append(latencyList.List, nodeLatency)
				latencyList.Unlock()
			}
			wg.Done()
		}(nodeIP.String())
	}
	wg.Wait()
	if len(latencyList.List) != 0 {
		sort.Sort(LowLatencyFirst(latencyList.List))
		return latencyList.List[0]
	}
	return types.NodeLatency{}
}

func Ping(nodeIP string, nodePort string, thisNodeID string) (types.NodeLatency, error) {
	var elapsed []float64
	var totalElapsed, totalDeviation float64
	var failed int
	var nodeID string
	for i := 0; i < 3; i++ {
		req, _ := http.NewRequest("GET", "http://"+nodeIP+":"+nodePort+"/api/v1/ping", nil)
		client := &http.Client{Timeout: 500 * time.Millisecond}
		start := time.Now()
		resp, err := client.Do(req)
		if err != nil {
			failed++
			continue
		}
		res, _ := ioutil.ReadAll(resp.Body)
		elapsedT := float64(time.Since(start)) / float64(time.Millisecond)
		elapsed = append(elapsed, elapsedT)
		totalElapsed += elapsedT
		if nodeID == "" {
			msg := types.APIRespond{}
			err := json.Unmarshal(res, &msg)
			if err != nil {
				return types.NodeLatency{}, errors.New("nope")
			}
			if msg.Result == thisNodeID {
				return types.NodeLatency{}, errors.New("nope")
			}
			nodeID = msg.Result.(string)
		}
	}
	if failed > 1 {
		return types.NodeLatency{NodeID: strings.Trim(nodeID, " ")}, errors.New("nope")
	}
	for j := 0; j < (3 - failed); j++ {
		totalDeviation += math.Abs((totalElapsed / 3) - elapsed[j])
	}
	return types.NodeLatency{
		NodeID:     strings.Trim(nodeID, " "),
		IP:         nodeIP,
		AvgLatency: totalElapsed / 3,
		AvgDev:     totalDeviation / 3,
	}, nil
}

type LowLatencyFirst []types.NodeLatency

func (latency LowLatencyFirst) Len() int      { return len(latency) }
func (latency LowLatencyFirst) Swap(i, j int) { latency[i], latency[j] = latency[j], latency[i] }
func (latency LowLatencyFirst) Less(i, j int) bool {
	if latency[i].AvgLatency+latency[i].AvgDev < latency[j].AvgLatency+latency[j].AvgDev {
		return true
	}
	return false
}
