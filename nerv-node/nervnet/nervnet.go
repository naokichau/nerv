package nervnet

import (
	gonet "net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/constvar"
	"github.com/naokichau/nerv/nerv-node/controller"
	"github.com/naokichau/nerv/nerv-node/nervnet/netutils"
	"github.com/naokichau/nerv/nerv-node/utils"

	"github.com/naokichau/nerv/nerv-node/logging"
)

type Nervnet struct {
	WorkerList     nodeList
	NodeList       nodeList
	ConnectedNodes nodeConnList
	ConnectedUser  userConnList
	NodeMsgCh      chan NetMsg
	Params         Params
}

type Params struct {
	NodeID     string
	InitNodes  []string
	Controller *controller.Controller
}

func (net *Nervnet) Init() {
	net.ConnectedUser.List = make(map[string]*userConnection)
	net.NodeList.Nodes = make(map[string]nodeInfo)
	net.ConnectedNodes.List = make(map[string]*nodeConn)
	net.NodeMsgCh = make(chan NetMsg)
	net.Params.NodeID = config.HostID
	net.NodeList.Nodes[net.Params.NodeID] = nodeInfo{
		Version:  constvar.NodeVersion(),
		Resource: utils.GetNodeResource(),
		Net:      config.Network,
	}
	time.AfterFunc(1*time.Second, net.startNetManagement)
	time.AfterFunc(2*time.Second, func() {
		logging.Log("Begin connect to init nodes...")
		for _, nodeIP := range net.Params.InitNodes {
			go func(nodeIP string) {
				node := strings.Split(nodeIP, "|")
				if node[0] != "" && indexOf(node[0], netutils.GetAllInterfaceIPs()) == -1 {
					var newNode nodeInfo
					newNode.Net.IPs = append(newNode.Net.IPs, gonet.ParseIP(node[0]))
					if newNode.Net.IPs[0] == nil {
						logging.Error(constvar.ErrInvalid, "Invalid node ip", nil)
						return
					}
					if len(node) == 1 {
						newNode.Net.NodePort = 9000
					} else {
						nport, err := strconv.Atoi(node[1])
						newNode.Net.NodePort = nport
						if err != nil {
							logging.Error(constvar.ErrInvalid, "Invalid node port", err)
							return
						}
					}
					net.joinRequest(newNode, "")
				}
			}(nodeIP)
		}
	})
	fileServer := http.FileServer(http.Dir(config.Data.DataDir + "/storage"))
	http.HandleFunc("/join_network_app", net.handleAppJoinRequest)
	http.HandleFunc("/join_network_node", net.handleNodeJoinRequest)
	http.HandleFunc("/api/", net.apiHandler)
	http.HandleFunc("/web/", net.webHandler)
	http.Handle("/assets/", http.StripPrefix("/assets", net.assetsHandler(fileServer)))
	logging.Fatal(constvar.ErrConn, "Failed to join network!", http.ListenAndServe(":"+strconv.Itoa(config.Data.NodePort), nil))
}

func indexOf(element string, data []gonet.IP) int {
	for k, v := range data {
		if element == v.String() {
			return k
		}
	}
	return -1
}
