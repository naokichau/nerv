package nervnet

import (
	"net/http"
	"regexp"

	"github.com/naokichau/nerv/nerv-node/app-runtime"
)

func (net *Nervnet) webHandler(w http.ResponseWriter, r *http.Request) {
	var validPath = regexp.MustCompile(`(?m)^/web/(\w+)/(main|((?:\w+/)*)(.+\.\w+))`)
	params := validPath.FindStringSubmatch(r.RequestURI)
	if params == nil {
		unknownAPI(w)
		return
	}
	switch string(r.Method) {
	case "GET":
		apprt.ServeWeb(params, r, w)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
	return
}
