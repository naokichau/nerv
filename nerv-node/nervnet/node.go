package nervnet

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/websocket"
	"github.com/naokichau/nerv/nerv-node/config"
	"github.com/naokichau/nerv/nerv-node/constvar"
	"github.com/naokichau/nerv/nerv-node/logging"
	"github.com/naokichau/nerv/nerv-node/nervnet/netutils"
	"github.com/naokichau/nerv/nerv-node/types"
	uuid "github.com/satori/go.uuid"
)

// func (nodeC *nodeConn) openSendCh() {
// 	for {
// 		select {
// 		case msg := <-nodeC.MsgCh:
// 			nodeC.Lock()
// 			msgBuf, err := json.Marshal(msg)
// 			if err != nil {
// 				nodeC.Unlock()
// 				continue
// 			}
// 			w, err := nodeC.conn.NextWriter(websocket.TextMessage)
// 			if err != nil {
// 				nodeC.Unlock()
// 				logging.Error(constvar.ErrConn, "can't connect", err)
// 				deleteConn(nodeC.latency.nodeID)
// 				return
// 			}
// 			w.Write(msgBuf)
// 			w.Close()
// 			nodeC.Unlock()
// 		}

// 	}
// }

func (nodeC *nodeConn) openReceiveCh() {
	for {
		_, messageReader, err := nodeC.conn.NextReader()
		if err != nil {
			logging.Warn("Lost connection with " + nodeC.latency.NodeID + " " + nodeC.conn.RemoteAddr().String())
			nodeC.NetCtrl.deleteConn(nodeC.latency.NodeID)
			return
		}
		msg := types.DataPkg{}
		msgBuf := new(bytes.Buffer)
		msgBuf.ReadFrom(messageReader)
		err = json.Unmarshal(msgBuf.Bytes(), &msg)
		if err != nil {
			fmt.Println("sdfsdF", err, string(msgBuf.Bytes()))
			return
		}
		go nodeC.ProcessNodeMsg(msg)
	}
}

func (nodeC *nodeConn) ProcessNodeMsg(msg types.DataPkg) {
	// if err := common.CheckMsg(&msg); err != nil {

	// }
	nodeC.NodeMsgCh <- NetMsg{
		Sender:     nodeC.latency.NodeID,
		SenderType: 0,
		MsgData:    msg,
	}
	return
}

func (net *Nervnet) SendMsgToNode(nodeID string, msg *types.DataPkg) {
	msgBuf, err := json.Marshal(&msg)
	if err != nil {
		return
	}
	if _, ok := net.ConnectedNodes.List[nodeID]; ok {
		w, err := net.ConnectedNodes.List[nodeID].conn.NextWriter(websocket.TextMessage)
		if err != nil {
			logging.Error(constvar.ErrConn, "can't connect", err)
			net.deleteConn(nodeID)
			return
		}
		w.Write(msgBuf)
		w.Close()
	}
}
func (net *Nervnet) BroadcastMsg(msg types.DataPkg, receivedFrom string) error {
	msgBuf, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	net.ConnectedNodes.Lock()
	for nodeID, nodeC := range net.ConnectedNodes.List {
		go func(id string, nodeC *nodeConn) {
			if id != receivedFrom {
				nodeC.Lock()
				w, err := nodeC.conn.NextWriter(websocket.TextMessage)
				if err != nil {
					nodeC.Unlock()
					return
				}
				w.Write(msgBuf)
				w.Close()
				nodeC.Unlock()
			}
		}(nodeID, nodeC)
	}
	net.ConnectedNodes.Unlock()
	return nil
}

func (net *Nervnet) ProcessEvent(nodeEventBuf *[]byte) {
	nodeEvent := types.NodeEvent{}
	err := json.Unmarshal(*nodeEventBuf, &nodeEvent)
	if err != nil {
		return
	}
	switch nodeEvent.Type {
	case 0:
		var tempNodeList map[string]nodeInfo
		err := json.Unmarshal(nodeEvent.Content, &tempNodeList)
		if err != nil {
			return
		}
		net.UpdateNodeList(tempNodeList)
		break
	}
	return
}

func (net *Nervnet) handleNodeJoinRequest(w http.ResponseWriter, r *http.Request) {
	if len(net.ConnectedNodes.List) == maxConnectedNodes {
		w.WriteHeader(http.StatusTeapot)
		return
	}
	nodeID, err := uuid.FromString(r.URL.Query().Get("id"))
	if err != nil {
		fmt.Printf("Something went wrong: %s", err)
	}
	if nodeID.String() == net.Params.NodeID {
		logging.Warn("err.. Why there are 2 me? -_-")
		return
	}
	nodePort := r.URL.Query().Get("nodeport")
	nodeIP := strings.Split(r.RemoteAddr, ":")[0]

	var upgrader = websocket.Upgrader{}
	nodeConnection := &nodeConn{}
	nodeConnection.latency.NodeID = nodeID.String()
	nodeConnection.conn, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		logging.Error(constvar.ErrUnknown, "can't upgrade connection with "+nodeID.String(), err)
		return
	}

	_, nodeList, err := net.exchangeInfo(nodeConnection, nodePort)
	if err != nil {
		logging.Error(constvar.ErrConn, "failed to exchange node information with "+nodeIP, err)
		return
	}
	// nodeConnection.MsgCh = make(chan types.DataPkg)
	go func() {
		nodeLatency := netutils.IsAcceptablePing(nodeList[nodeID.String()].Net.IPs, nodePort, net.Params.NodeID)
		if nodeLatency.IP == "" {
			logging.Warn("connection with node " + nodeID.String() + " is one-way, can't ping back ⚠️  ")
		}
	}()
	nodeConnection.latency.NodeID = nodeID.String()
	nodeConnection.NodeMsgCh = net.NodeMsgCh
	nodeConnection.NetCtrl = net
	go nodeConnection.openReceiveCh()
	// go nodeConnection.openSendCh()
	net.NodeList.Lock()
	net.ConnectedNodes.Lock()
	net.ConnectedNodes.List[nodeID.String()] = nodeConnection
	net.NodeList.Nodes[nodeID.String()] = nodeList[nodeID.String()]
	net.NodeList.Unlock()
	net.ConnectedNodes.Unlock()

	// for {
	// 	select {
	// 	case <-nodeConnection.disconnect:
	// 		nodeConnection.disconnect = nil
	// 		go func() {
	// 			nodeConnection.conn.Close()
	// 			deleteConn(nodeID.String())
	// 		}()
	// 		return
	// 		// case <-interrupt:
	// 		// 	log.Println("interrupt")
	// 		// 	err := ConnectedNodes[newNode.IP.String()].conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	// 		// 	if err != nil {
	// 		// 		log.Println("write close:", err)
	// 		// 		return
	// 		// 	}
	// 		// 	select {
	// 		// 	case <-done:
	// 		// 	case <-time.After(time.Second):
	// 		// 	}
	// 		// 	return
	// 	}
	// }
}

func (net *Nervnet) joinRequest(node nodeInfo, specificIP string) {
	var err error
	var nodeLatency types.NodeLatency
	var okIP string
	nodePort := strconv.Itoa(node.Net.NodePort)
	if specificIP == "" {
		nodeLatency = netutils.IsAcceptablePing(node.Net.IPs, nodePort, net.Params.NodeID)
		if nodeLatency.IP == "" {
			logging.Error(constvar.ErrInvalid, "unacceptable latency", errors.New("unacceptable latency"))
			fmt.Println(node)
			return
		}
		okIP = nodeLatency.IP
		fmt.Println("sdfsdfsdfdsf wrwe", nodeLatency.NodeID)
	} else {
		okIP = specificIP
	}

	u := url.URL{Scheme: "ws", Host: okIP + ":" + nodePort, Path: "/join_network_node", RawQuery: "id=" + net.Params.NodeID + "&nodeport=" + strconv.Itoa(config.Data.NodePort)}
	logging.NetLog("connecting to " + u.String())

	nodeConnection := &nodeConn{latency: nodeLatency}
	nodeConnection.conn, _, err = websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		logging.Error(constvar.ErrConn, "node "+nodeLatency.NodeID+" ping OK but can't connect", err)
		return
	}

	nodeID, nodeList, err := net.exchangeInfo(nodeConnection, nodePort)
	if err != nil {
		logging.Error(constvar.ErrConn, "failed to exchange node information with "+okIP, err)
		nodeConnection.conn.Close()
		return
	}
	if nodeConnection.latency.NodeID == "" {
		nodeConnection.latency.NodeID = nodeID
	} else {

	}
	// nodeConnection.MsgCh = make(chan types.DataPkg)
	go nodeConnection.openReceiveCh()
	// go nodeConnection.openSendCh()
	nodeConnection.NetCtrl = net
	nodeConnection.NodeMsgCh = net.NodeMsgCh
	net.NodeList.Lock()
	net.ConnectedNodes.Lock()
	net.NodeList.Nodes[nodeID] = nodeList[nodeID]
	net.ConnectedNodes.List[nodeID] = nodeConnection
	net.NodeList.Unlock()
	net.ConnectedNodes.Unlock()
}

func (net *Nervnet) exchangeInfo(nodeC *nodeConn, nodeport string) (string, map[string]nodeInfo, error) {
	type exchangeNodeInfo struct {
		ID       string
		NodeList map[string]nodeInfo
	}

	infoReceived := make(chan bool)
	msgData := exchangeNodeInfo{}
	go func() {
		for {
			mt, message, err := nodeC.conn.ReadMessage()
			if err != nil {
				log.Println("read: sadasds", mt, err)
				return
			}
			err = json.Unmarshal(message, &msgData)
			if err != nil {
				fmt.Println(err)
			} else {
				infoReceived <- true
			}
			return
		}
	}()
	go func() {
		net.NodeList.Lock()
		msg := &exchangeNodeInfo{
			ID:       net.Params.NodeID,
			NodeList: net.NodeList.Nodes,
		}
		msgBuf, err := json.Marshal(msg)
		net.NodeList.Unlock()
		nodeC.Lock()
		err = nodeC.conn.WriteMessage(websocket.TextMessage, msgBuf)
		nodeC.Unlock()
		if err != nil {
			log.Println("write:", err)
			logging.Error(constvar.ErrConn, "can't connect", err)
		}
	}()
	select {
	case <-infoReceived:
		fmt.Println("Information exchanged with " + msgData.ID)
		return msgData.ID, msgData.NodeList, nil
	case <-time.After(2 * time.Second):
		return "", make(map[string]nodeInfo), errors.New("didn't receive node infomation. Timeout!")
	}
}

func (net *Nervnet) deleteConn(nodeID string) {
	logging.NetLog("Disconnect node " + nodeID)
	net.ConnectedNodes.Lock()
	defer net.ConnectedNodes.Unlock()
	if _, ok := net.ConnectedNodes.List[nodeID]; ok {
		net.NodeList.Lock()
		net.ConnectedNodes.List[nodeID].Lock()
		// ConnectedNodes.List[nodeID].MsgCh = nil
		net.ConnectedNodes.List[nodeID].conn.Close()
		net.ConnectedNodes.List[nodeID].Unlock()
		delete(net.ConnectedNodes.List, nodeID)
		delete(net.NodeList.Nodes, nodeID)
		net.NodeList.Unlock()
	} else {
		logging.Warn("Can't disconnect node that isn't connected")
	}
}
