package nervnet

import (
	"net/http"
	"strings"
)

// TODO:
// + Check ACL of asset

func (net *Nervnet) assetsHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "GET" {
			if strings.HasSuffix(r.URL.Path, "/") {
				http.NotFound(w, r)
				return
			}

			assetLink, err := net.Params.Controller.GetAssetLink(r.URL.Path[1:])
			if err != nil {
				http.NotFound(w, r)
			}
			r.URL.Path = assetLink
			next.ServeHTTP(w, r)
		} else {
			http.Error(w, "Invalid file request", 400)
			return
		}
	})
}
