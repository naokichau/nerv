package nervnet

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"github.com/naokichau/nerv/nerv-node/constvar"
	"github.com/naokichau/nerv/nerv-node/database/cachedb"
	"github.com/naokichau/nerv/nerv-node/logging"
	"github.com/naokichau/nerv/nerv-node/types"
)

func (net *Nervnet) handleAppJoinRequest(w http.ResponseWriter, r *http.Request) {
	userConn := &userConnection{}
	var err error
	// #TODO check app params before accept
	// if !validator.AppJoinChecker() {
	// 	w.WriteHeader(http.StatusNotAcceptable)
	// 	return
	// }
	userAppID := r.URL.Query().Get("id")
	appID := r.URL.Query().Get("app")
	err = cachedb.AppUserCache.Set(userAppID, appID, 0, 0)
	if err != nil {
		return
	}
	var upgrader = websocket.Upgrader{}
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	userConn.conn, err = upgrader.Upgrade(w, r, nil)
	if err != nil {
		logging.Error(constvar.ErrUnknown, "can't upgrade connection with "+userAppID, err)
		return
	}

	userConn.MsgCh = make(chan types.DataPkg)
	userConn.userAppID = userAppID
	userConn.AppID = appID
	net.ConnectedUser.Lock()
	net.ConnectedUser.List[userAppID] = userConn
	net.ConnectedUser.Unlock()
	logging.Log("User " + userAppID + " connected")
	time.AfterFunc(2*time.Second, func() {
		fmt.Println(userAppID)
		msgList, _, err := cachedb.UserMsgCache.Get(userAppID)
		if err != nil {
			fmt.Println(err)
			return
		}
		for _, msg := range msgList.([]interface{}) {
			userConn.MsgCh <- msg.(types.DataPkg)
		}
		cachedb.UserMsgCache.Del(userAppID)
	})

	go func() {
		for {
			msg := <-userConn.MsgCh
			msgBuf, err := json.Marshal(msg)
			if err != nil {
				continue
			}
			w, err := userConn.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				logging.Error(constvar.ErrConn, "can't connect", err)
				net.deleteUserConn(userAppID)
				return
			}
			w.Write(msgBuf)
			w.Close()
		}
	}()
	for {
		_, messageReader, err := userConn.conn.NextReader()
		if err != nil {
			logging.Warn("Lost connection with " + userAppID)
			net.deleteUserConn(userAppID)
			return
		}
		msg := types.DataPkg{}
		msgBuf := new(bytes.Buffer)
		msgBuf.ReadFrom(messageReader)
		err = json.Unmarshal(msgBuf.Bytes(), &msg)
		if err != nil {
			fmt.Println(err, string(msgBuf.Bytes()))
			return
		}
		go userConn.ProcessAppMsg(msg)
	}
}

func (userConn *userConnection) ProcessAppMsg(msg types.DataPkg) {
	userConn.NodeMsgCh <- NetMsg{
		Sender:     userConn.userAppID,
		SenderType: 1,
		MsgData:    msg,
	}
	return
}

func (net *Nervnet) deleteUserConn(userAppID string) {
	logging.NetLog("Disconnect user " + userAppID)
	net.ConnectedUser.Lock()
	cachedb.AppUserCache.Del(userAppID)
	if _, ok := net.ConnectedUser.List[userAppID]; ok {
		net.ConnectedUser.List[userAppID].MsgCh = nil
		delete(net.ConnectedUser.List, userAppID)
	}
	net.ConnectedUser.Unlock()
}
