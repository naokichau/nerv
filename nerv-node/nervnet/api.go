package nervnet

import (
	"errors"
	"net/http"
	"regexp"

	"github.com/naokichau/nerv/nerv-node/types"
)

func writeRes(w http.ResponseWriter, res string, err error) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	var result []byte
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		result, _ = types.APIRespond{
			Result: "Error when processing request! Error: " + err.Error(),
		}.Marshal()
	} else {
		w.WriteHeader(http.StatusOK)
		result, _ = types.APIRespond{
			Result: res,
		}.Marshal()
	}
	w.Write(result)
}
func checkStatus() (string, error) {
	return "", errors.New("Ops... This api isn't available yet! ")
}

func unknownAPI(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusBadRequest)
	result, _ := types.APIRespond{
		Result: "Unknown api not found!",
	}.Marshal()
	w.Write(result)
}

func getAsset(r *http.Request) (string, error) {
	// add support for parallel download

	return "", errors.New("nope")
}

func appList(r *http.Request) (string, error) {
	return "testing app", nil
	return "", errors.New("nope")
}

func getPolicy(r *http.Request) (string, error) {

	return "", errors.New("nope")
}
func (net *Nervnet) deployAppRes(r *http.Request) (string, error) {
	result, err := net.Params.Controller.Deploy(r)
	if err != nil {
		return "", err
	}
	return result, nil
}

func (net *Nervnet) apiHandler(w http.ResponseWriter, r *http.Request) {
	var validPath = regexp.MustCompile(`(?m)^/api/v([1])/([a-z]+)(\?\w+\=.+|)`)
	params := validPath.FindStringSubmatch(r.RequestURI)
	if params == nil {
		unknownAPI(w)
		return
	}
	switch string(r.Method) {
	case "GET":
		switch params[2] {
		case "ping":
			writeRes(w, net.Params.NodeID, nil)
		case "status":
			res, err := checkStatus()
			writeRes(w, res, err)
		case "apps":
			res, err := appList(r)
			writeRes(w, res, err)
		case "app-policy":
			res, err := getPolicy(r)
			writeRes(w, res, err)
		default:
			w.WriteHeader(http.StatusNotAcceptable)
		}
	case "POST":
		switch params[2] {
		case "deployapp":
			res, err := net.deployAppRes(r)
			writeRes(w, res, err)
		default:
			w.WriteHeader(http.StatusNotAcceptable)
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
	return
}
