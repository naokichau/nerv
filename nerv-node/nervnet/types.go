package nervnet

import (
	"sync"

	"github.com/gorilla/websocket"
	"github.com/naokichau/nerv/nerv-node/types"
)

// msgType:
// 0: node event
// 1: query
// 2: send to
// 3: function call
// 4: procedure call
// 5: service call
// 6: respond msg

// RequestorType:
// 0: node
// 1: user

// RequestType:
// 1: query (assets)
// 3: function call (app)
// 4: procedure call (app)
// 5: service call (app)

// OwnerType:
// 0: user
// 1: app

type nodeConn struct {
	conn      *websocket.Conn
	NodeMsgCh chan NetMsg
	NetCtrl   *Nervnet
	latency   types.NodeLatency
	sync.Mutex
}

type nodeInfo struct {
	Net       types.NodeNetworkInfo
	Version   string
	Resource  types.NodeResource
	Owner     string
	Reachable bool
}

type RequestMsg struct {
	RequestorType int
	RequestType   int
	Requestor     string
	RequestCtx    interface{}
}

type NetMsg struct {
	Sender     string
	SenderType int
	MsgData    types.DataPkg
}

type nodeList struct {
	Nodes map[string]nodeInfo
	sync.Mutex
}
type worker struct {
	ID       string
	Version  string
	Workload string
	Resource types.NodeResource
}

type nodeConnList struct {
	List map[string]*nodeConn
	sync.Mutex
}

type userConnList struct {
	List map[string]*userConnection
	sync.Mutex
}

type MsgList struct {
	List map[string]*types.DataPkg
	sync.Mutex
}

type RespondMsg struct {
	Msg    types.DataPkg
	Sender string
}
type WaitForRepsondMsgList struct {
	List    map[string]*RequestMsg
	Respond chan RespondMsg
	sync.Mutex
}

type userConnection struct {
	conn      *websocket.Conn
	userAppID string
	AppID     string
	MsgCh     chan types.DataPkg
	NodeMsgCh chan NetMsg
}

func (list *WaitForRepsondMsgList) CheckExist(ID string) bool {
	list.Lock()
	defer list.Unlock()
	if _, ok := list.List[ID]; ok {
		return true
	}
	return false
}

func (list *MsgList) CheckExist(ID string) bool {
	list.Lock()
	defer list.Unlock()
	if _, ok := list.List[ID]; ok {
		return true
	}
	return false
}

func (list *nodeConnList) CheckExist(ID string) bool {
	list.Lock()
	defer list.Unlock()
	if _, ok := list.List[ID]; ok {
		return true
	}
	return false
}

func (list *userConnList) CheckExist(ID string) bool {
	list.Lock()
	defer list.Unlock()
	if _, ok := list.List[ID]; ok {
		return true
	}
	return false
}
