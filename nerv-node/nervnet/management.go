package nervnet

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"sync"
	"time"

	"github.com/naokichau/nerv/nerv-node/logging"
	"github.com/naokichau/nerv/nerv-node/nervnet/netutils"
	"github.com/naokichau/nerv/nerv-node/types"
	uuid "github.com/satori/go.uuid"
)

const (
	broadcastListInterval time.Duration = 5
	maxConnectedNodes                   = 10
	minConnectedNodes                   = 8
)

func (net *Nervnet) startNetManagement() {
	logging.Log("Starting network management...")
	// net.UpdateNodeList = make(chan bool)
	ticker := time.NewTicker(broadcastListInterval * time.Second)
	// go func() {
	// 	for {
	// 		select {
	// 		case <-net.UpdateNodeList:
	// 			net.NodeList.Lock()
	// 			net.ConnectedNodes.Lock()
	// 			logging.NetLog("Known node list has been updated! There are " + strconv.Itoa(len(net.NodeList.Nodes)) + " known nodes and currently connected to " + strconv.Itoa(len(net.ConnectedNodes.List)))
	// 			net.NodeList.Unlock()
	// 			net.ConnectedNodes.Unlock()
	// 		}
	// 	}
	// }()
	for {
		select {
		case <-ticker.C:
			logging.NetLog("Recalibrating connection to nodes...")
			nodeList := net.NodeList.Nodes
			net.recalibrateConnection(nodeList)
			net.NodeList.Lock()
			logging.NetLog("Begin purging node list...")
			for node := range net.NodeList.Nodes {
				if _, ok := net.ConnectedNodes.List[node]; !ok && node != net.Params.NodeID {
					delete(net.NodeList.Nodes, node)
				}
			}
			net.NodeList.Unlock()
			logging.NetLog("Broadcasting node list")
			nodeListByte, _ := json.Marshal(net.NodeList.Nodes)
			msg, _ := json.Marshal(types.NodeEvent{Type: 0,
				Content: nodeListByte})
			net.BroadcastMsg(types.DataPkg{
				Data: types.MsgContent{
					ID:         uuid.Must(uuid.NewV4()).String(),
					ActionType: 0,
					Content:    msg,
					SendTime:   time.Now().String(),
				},
			}, "")
		}
	}
}

func (net *Nervnet) recalibrateConnection(nodeList map[string]nodeInfo) {
	type nodesLatency struct {
		latencies []types.NodeLatency
		sync.Mutex
	}
	var nodesLC nodesLatency
	var wg sync.WaitGroup
	for nodeID, node := range nodeList {
		wg.Add(1)
		go func(id string, info nodeInfo) {
			defer wg.Done()
			if id != net.Params.NodeID {
				lat := netutils.IsAcceptablePing(info.Net.IPs, strconv.Itoa(info.Net.NodePort), net.Params.NodeID)
				if lat.IP == "" {
					net.ConnectedNodes.Lock()
					if _, ok := net.ConnectedNodes.List[id]; ok {
						logging.Warn("connection with node " + id + " is one-way, can't ping back ⚠️  ")
					} else {
						logging.Warn("Node " + id + " doesn't satified ping time")
						// fmt.Println(info.Net.IPs, strconv.Itoa(info.Net.NodePort))
					}
					net.ConnectedNodes.Unlock()
					lat.NodeID = id
					lat.AvgLatency = 999
					lat.AvgDev = 999
					nodesLC.Lock()
					nodesLC.latencies = append(nodesLC.latencies, lat)
					nodesLC.Unlock()
					return
				}
				if lat.NodeID != id {
					fmt.Println(lat.NodeID, id)
					logging.Warn("different nodeID but same nodeIP, will delete " + id)
					delete(net.NodeList.Nodes, id)
					return
				}
				fmt.Println(id, lat.IP, strconv.Itoa(info.Net.NodePort), fmt.Sprintf("%.3fms +-%.3f", lat.AvgLatency, lat.AvgDev))
				nodesLC.Lock()
				nodesLC.latencies = append(nodesLC.latencies, lat)
				nodesLC.Unlock()
			}
			return
		}(nodeID, node)
	}
	wg.Wait()
	sort.Sort(netutils.LowLatencyFirst(nodesLC.latencies))
	for idx, nodeLatency := range nodesLC.latencies {
		net.ConnectedNodes.Lock()
		if idx > minConnectedNodes-1 {
			if len(net.ConnectedNodes.List) < minConnectedNodes {
				if _, ok := net.ConnectedNodes.List[nodeLatency.NodeID]; !ok {
					net.ConnectedNodes.Unlock()
					go net.joinRequest(net.NodeList.Nodes[nodeLatency.NodeID], nodeLatency.IP)
					continue
				} else {
					net.ConnectedNodes.List[nodeLatency.NodeID].Lock()
					net.ConnectedNodes.List[nodeLatency.NodeID].latency = nodeLatency
					net.ConnectedNodes.List[nodeLatency.NodeID].Unlock()
					net.ConnectedNodes.Unlock()
					continue
				}
			}
			if len(net.ConnectedNodes.List) > minConnectedNodes {
				if _, ok := net.ConnectedNodes.List[nodeLatency.NodeID]; ok {
					net.ConnectedNodes.Unlock()
					net.deleteConn(nodeLatency.NodeID)
				} else {
					net.ConnectedNodes.Unlock()
				}
			}
		} else {
			if _, ok := net.ConnectedNodes.List[nodeLatency.NodeID]; ok {
				if nodeLatency.AvgLatency == 999 {
					net.ConnectedNodes.Unlock()
					net.deleteConn(nodeLatency.NodeID)
					continue
				}
				net.ConnectedNodes.List[nodeLatency.NodeID].Lock()
				net.ConnectedNodes.List[nodeLatency.NodeID].latency = nodeLatency
				net.ConnectedNodes.List[nodeLatency.NodeID].Unlock()
				net.ConnectedNodes.Unlock()
			} else {
				net.ConnectedNodes.Unlock()
				if nodeLatency.AvgLatency != 999 {
					go net.joinRequest(net.NodeList.Nodes[nodeLatency.NodeID], nodeLatency.IP)
				}
			}
		}
	}
	logging.NetLog("Done recalibrating")
}

func (net *Nervnet) UpdateNodeList(tempNodeList map[string]nodeInfo) {
	for k, v := range tempNodeList {
		net.NodeList.Lock()
		net.NodeList.Nodes[k] = v
		net.NodeList.Unlock()
	}
}
