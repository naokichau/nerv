#!/bin/sh
yes | pacman -S systemd git go python make wget gcc squashfs-tools
VERSION=2.4.2
wget https://github.com/singularityware/singularity/releases/download/$VERSION/singularity-$VERSION.tar.gz
tar xvf singularity-$VERSION.tar.gz
cd singularity-$VERSION
./configure --prefix=/usr/local
make
make install
cd ..
rm -rf singularity
yes | pacman -R make wget gcc